package edu.ssau.acat.web.prediction;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.impl.conformation.dto.MorseConformations;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.service.prediction.ClusterPredictionService;

@Controller
public class PredictionServiceController {

    private ClusterPredictionService clusterPredictionService;

    @Autowired
    public PredictionServiceController(ClusterPredictionService clusterPredictionService) {
        this.clusterPredictionService = clusterPredictionService;
    }

    @RequestMapping(value = "/start", method = RequestMethod.GET)
    public String startService() {
        // Creates root conformations to start search from
        MorseClusterConformation morseCluster7A = MorseConformations.createMorseConformation7A();
        MorseClusterConformation morseCluster7XB = MorseConformations.createMorseConformation7XB();
        List<ClusterConformation> rootConformations = new ArrayList<>();
        rootConformations.add(morseCluster7A);
        rootConformations.add(morseCluster7XB);

        clusterPredictionService.start(7, 32, rootConformations);

        return "dummy";
    }
}
