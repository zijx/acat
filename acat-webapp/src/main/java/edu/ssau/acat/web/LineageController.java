package edu.ssau.acat.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.ssau.acat.model.lineage.Lineage;
import edu.ssau.acat.service.persistence.LineagePersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/lineage")
public class LineageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LineageController.class);

    private final LineagePersistenceService service;

    @Autowired LineageController(LineagePersistenceService service) {
        this.service = service;
    }

    @RequestMapping(value = "{n}", method = RequestMethod.DELETE) Lineage delete(@PathVariable("n") int n) {
        LOGGER.info("Deleting lineage with n: {}", n);
        Lineage deleted = service.delete(n);
        LOGGER.info("Deleted lineage: {}", deleted);
        return deleted;
    }

    @RequestMapping(method = RequestMethod.GET) List<Lineage> findAll() {
        LOGGER.info("Finding all lineage entries");
        List<Lineage> lineageList = service.findAll();
        LOGGER.info("Found {} lineage entries", lineageList.size());
        return lineageList;
    }

    @RequestMapping(value = "{n}", method = RequestMethod.GET) Lineage findByNumberOfAtoms(
            @PathVariable("n") int n) {
        LOGGER.info("Finding lineage with n: {}", n);
        Lineage lineage = service.findByNumberOfAtoms(n);
        LOGGER.info("Found lineage {}.", lineage);
        return lineage;
    }
}
