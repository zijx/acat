package edu.ssau.acat.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.ssau.acat.model.conformation.FilteredConformation;
import edu.ssau.acat.service.persistence.ConformationPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/filtered")
public class FilteredConformationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FilteredConformationController.class);

    private final ConformationPersistenceService service;

    @Autowired FilteredConformationController(ConformationPersistenceService service) {
        this.service = service;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE) FilteredConformation delete(@PathVariable("id") String id) {
        LOGGER.info("Deleting conformation entry with id: {}", id);
        FilteredConformation deleted = service.deleteFiltered(id);
        LOGGER.info("Deleted conformation entry: {}", deleted);
        return deleted;
    }

    @RequestMapping(method = RequestMethod.GET) List<FilteredConformation> findAll() {
        LOGGER.info("Finding all conformation entries");
        List<FilteredConformation> conformations = service.findAllFiltered();
        LOGGER.info("Found {} conformation entries", conformations.size());
        return conformations;
    }

    @RequestMapping(value = "{n}", method = RequestMethod.GET) List<FilteredConformation> findByNumberOfAtoms(
            @PathVariable("n") int n) {
        LOGGER.info("Finding conformations with n: {}", n);
        List<FilteredConformation> conformations = service.findFilteredByNumberOfAtoms(n);
        LOGGER.info("Found {} conformations.", conformations.size());
        return conformations;
    }
}
