package edu.ssau.acat;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    private static final String VIEWS = "templates/";

    //private static final String MESSAGE_SOURCE = "/WEB-INF/i18n/messages";
    private static final String RESOURCES_HANDLER = "/resources/";
    private static final String RESOURCES_LOCATION = RESOURCES_HANDLER + "**";

    // Configures ViewResolvers provided by Thymeleafe
    // These components are picked automatically - no need to register
    // org.thymeleafe ViewResolver and SpringTemplateEngine


//@Bean
//    public TemplateResolver templateResolver() {
//        TemplateResolver templateResolver = new ServletContextTemplateResolver();
//        templateResolver.setPrefix(VIEWS);
//        templateResolver.setSuffix(".html");
//        templateResolver.setTemplateMode("HTML5");
//        templateResolver.setCacheable(false);
//        return templateResolver;
//    }

//    @Bean
//    public UrlTemplateResolver urlTemplateResolver() {
//        return new UrlTemplateResolver();
//    }

    // Resolves links similar to xml-way:
    //  <mvc:view-controller path="/" view-name="home"/>
    //  <mvc:view-controller path="/admin" view-name="admin/admin"/>
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addViewController("/").setViewName("home/home");
        registry.addViewController("/dashboard").setViewName("dashboard/standalone");
    }

//    @Bean(name = "messageSource")
//    public MessageSource messageSource() {
//        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//        messageSource.setBasename(MESSAGE_SOURCE);
//        messageSource.setCacheSeconds(5);
//        return messageSource;
//    }

//    @Override
//    public Validator getValidator() {
//        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
//        validator.setValidationMessageSource(messageSource());
//        return validator;
//    }


    // All resources inside folder src/main/resources are mapped so they can be referred to inside template files
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler(RESOURCES_HANDLER).addResourceLocations(RESOURCES_LOCATION);
//    }


    // -------------------------------------------------------------------------
    // Configures a request handler for serving static resources
    // by forwarding the request to the Servlet container's "default" Servlet.
    // -------------------------------------------------------------------------
//    @Override
//    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
//    }

}
