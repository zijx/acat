package edu.ssau.acat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
/*@Configuration
@EnableAutoConfiguration
@ComponentScan*/
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
