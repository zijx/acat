package edu.ssau.acat.exception;

public class DomainObjectNotFoundException extends RuntimeException {

    public DomainObjectNotFoundException(String id) {
        super(String.format("No object found with id: <%s>", id));
    }
}
