package edu.ssau.acat.service.persistence.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ssau.acat.model.lineage.Lineage;
import edu.ssau.acat.repository.LineageRepository;
import edu.ssau.acat.service.persistence.LineagePersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
final class LineagePersistenceServiceImpl implements LineagePersistenceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LineagePersistenceServiceImpl.class);

    private final LineageRepository repository;

    @Autowired
    public LineagePersistenceServiceImpl(LineageRepository repository) {
        this.repository = repository;
    }

    @Override
    public Lineage insert(Lineage lineage) {
        LOGGER.trace("Inserting lineage {} ", lineage);
        Lineage inserted = repository.insert(lineage);
        LOGGER.trace("Inserted lineage {}", lineage);
        return inserted;
    }

    @Override
    public Lineage delete(int n) {
        LOGGER.trace("Deleting lineage with n: {}", n);
        Lineage deleted = findByNumberOfAtoms(n);
        repository.delete(deleted);
        LOGGER.trace("Deleted lineage: {}", deleted);
        return deleted;
    }

    @Override
    public List<Lineage> findAll() {
        LOGGER.trace("Finding all lineage entries.");
        List<Lineage> lineageList = repository.findAll();
        LOGGER.trace("Found {} lineage entries", lineageList.size());
        return lineageList;
    }

    @Override
    public Lineage findByNumberOfAtoms(int n) {
        LOGGER.trace("Finding lineage with n: {}", n);
        Lineage lineage = repository.findByN(n);
        LOGGER.trace("Found lineage {}.", lineage);
        return lineage;
    }
}
