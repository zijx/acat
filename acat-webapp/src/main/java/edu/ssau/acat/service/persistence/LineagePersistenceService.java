package edu.ssau.acat.service.persistence;

import java.util.List;

import edu.ssau.acat.model.lineage.Lineage;

/**
 * Service to perform CRUD operations on {@code Lineage} objects.
 */
public interface LineagePersistenceService {

    Lineage insert(Lineage conformation);

    Lineage delete(int n);

    List<Lineage> findAll();

    Lineage findByNumberOfAtoms(int n);
}
