package edu.ssau.acat.service.prediction;

import java.util.Collection;
import java.util.List;

import edu.ssau.acat.core.api.conformation.ClusterConformation;

public interface ClusterPredictionService {

    void start(int startingDimension, int targetDimension, List<ClusterConformation> rootConformations);

    @SuppressWarnings("unchecked")
    void startFromNodes(Collection<? extends ClusterConformation> children, int targetDimension);
}
