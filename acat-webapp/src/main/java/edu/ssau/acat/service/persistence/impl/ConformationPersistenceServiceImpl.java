package edu.ssau.acat.service.persistence.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ssau.acat.model.conformation.FilteredConformation;
import edu.ssau.acat.model.conformation.SelectedConformation;
import edu.ssau.acat.repository.FilteredConformationRepository;
import edu.ssau.acat.repository.SelectedConformationRepository;
import edu.ssau.acat.exception.DomainObjectNotFoundException;
import edu.ssau.acat.service.persistence.ConformationPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
final class ConformationPersistenceServiceImpl implements ConformationPersistenceService {

    public static final String DEFAULT_DIRECTION = "desc";

    private static final Logger LOGGER = LoggerFactory.getLogger(ConformationPersistenceServiceImpl.class);

    private final SelectedConformationRepository selectedRepository;
    private final FilteredConformationRepository filteredRepository;

    @Value("${spring.data.rest.default-page-size}")
    private int defaultPageSize;

    @Autowired
    ConformationPersistenceServiceImpl(SelectedConformationRepository selectedRepository,
            FilteredConformationRepository filteredRepository) {
        this.selectedRepository = selectedRepository;
        this.filteredRepository = filteredRepository;
    }

    @Override
    public SelectedConformation insertSelected(SelectedConformation conformation) {
        LOGGER.info("Inserting selected conformation");
        SelectedConformation inserted = selectedRepository.insert(conformation);
        LOGGER.info("Inserted selected conformation {}", conformation);
        return inserted;
    }

    @Override
    public FilteredConformation insertFiltered(FilteredConformation conformation) {
        LOGGER.info("Inserting filtered conformation");
        FilteredConformation inserted = filteredRepository.insert(conformation);
        LOGGER.info("Inserted filtered conformation {}", conformation);
        return inserted;
    }

    @Override
    public int insertAllSelected(Collection<SelectedConformation> conformations) {
        LOGGER.info("Inserting {} selected conformations", conformations.size());
        List<SelectedConformation> inserted = selectedRepository.insert(conformations);
        LOGGER.info("Inserted {} selected conformations", inserted.size());
        return inserted.size();
    }

    @Override public int insertAllFiltered(Collection<FilteredConformation> conformations) {
        LOGGER.info("Inserting {} filtered conformations", conformations.size());
        List<FilteredConformation> inserted = filteredRepository.insert(conformations);
        LOGGER.info("Inserted {} filtered conformations", inserted.size());
        return inserted.size();
    }

    @Override
    public SelectedConformation deleteSelected(String id) {
        LOGGER.info("Deleting a selected conformation entry with id: {}", id);
        SelectedConformation deleted = findSelectedById(id);
        selectedRepository.delete(deleted);
        LOGGER.info("Deleted selected conformation entry: {}", deleted);
        return deleted;
    }

    @Override
    public FilteredConformation deleteFiltered(String id) {
        LOGGER.info("Deleting a filtered conformation entry with id: {}", id);
        FilteredConformation deleted = findFilteredById(id);
        filteredRepository.delete(deleted);
        LOGGER.info("Deleted filtered conformation entry: {}", deleted);
        return deleted;
    }

    // MongoRepository implements PagingAndSortingRepository
    @Override
    public List<SelectedConformation> findAllSelected(Integer pageSize, List<String> properties) {
        if (pageSize == null) {
            pageSize = defaultPageSize;
        }
        if (properties == null) {
            return findAllSelectedPaged(pageSize);
        }
        return findAllSelectedPagedAndSorted(pageSize, properties);
    }

    @Override
    public List<SelectedConformation> findAllSelected() {
        LOGGER.info("Finding all selected conformation entries.");
        List<SelectedConformation> conformations = selectedRepository.findAll();
        LOGGER.info("Found {} selected conformation entries", conformations.size());
        return conformations;
    }

    @Override
    public List<FilteredConformation> findAllFiltered() {
        LOGGER.info("Finding all filtered conformation entries.");
        List<FilteredConformation> conformations = filteredRepository.findAll();
        LOGGER.info("Found {} filtered conformation entries", conformations.size());
        return conformations;
    }

    @Override
    public List<SelectedConformation> findSelectedByNumberOfAtoms(int n) {
        LOGGER.info("Finding selected conformations with n: {}", n);
        List<SelectedConformation> conformations = selectedRepository.findByN(n);
        LOGGER.info("Found {} selected conformations.", conformations.size());
        return conformations;
    }

    @Override
    public List<SelectedConformation> findAllSelectedGlobalMinima() {
        LOGGER.info("Finding selected GO conformations");
        List<SelectedConformation> conformations = selectedRepository.findByIsGOIsTrue();
        LOGGER.info("Found {} selected GO conformations", conformations.size());
        return conformations;
    }

    @Override
    public List<FilteredConformation> findFilteredByNumberOfAtoms(int n) {
        LOGGER.info("Finding filtered conformations with n: {}", n);
        List<FilteredConformation> conformations = filteredRepository.findByN(n);
        LOGGER.info("Found {} filtered conformations.", conformations.size());
        return conformations;
    }

    private List<SelectedConformation> findAllSelectedPaged(int pageSize) {
        LOGGER.info("Fetching {} selected conformations within a single page", pageSize);
        Page<SelectedConformation> selectedConformationPage = selectedRepository.findAll(new PageRequest(0, pageSize));
        LOGGER.info("All conformations that can be fetched: {}", selectedConformationPage.getTotalElements());
        return selectedConformationPage.getContent();
    }

    private List<SelectedConformation> findAllSelectedPagedAndSorted(int pageSize, List<String> properties) {
        LOGGER.info("Finding all selected conformations: {} within a single page and sorted by {}", pageSize, properties);
        Page<SelectedConformation> selectedConformationPage = selectedRepository.findAll(new PageRequest(0, pageSize,
                        Sort.Direction.fromString(DEFAULT_DIRECTION), properties.toArray(new String[0]))
        );
        LOGGER.info("Fetched {} selected conformations", selectedConformationPage.getTotalElements());
        return selectedConformationPage.getContent();
    }

    private SelectedConformation findSelectedById(String id) {
        SelectedConformation conformation = selectedRepository.findOne(id);
        if (conformation == null) {
            throw new DomainObjectNotFoundException(id);
        }
        return conformation;
    }

    private FilteredConformation findFilteredById(String id) {
        FilteredConformation conformation = filteredRepository.findOne(id);
        if (conformation == null) {
            throw new DomainObjectNotFoundException(id);
        }
        return conformation;
    }
}
