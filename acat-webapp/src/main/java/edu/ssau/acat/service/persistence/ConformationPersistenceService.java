package edu.ssau.acat.service.persistence;

import java.util.Collection;
import java.util.List;

import edu.ssau.acat.model.conformation.FilteredConformation;
import edu.ssau.acat.model.conformation.SelectedConformation;

/**
 * Service to perform CRUD operations on {@code SelectedConformation} and {@code FilteredConformation} objects.
 */
public interface ConformationPersistenceService {

    SelectedConformation insertSelected(SelectedConformation conformation);

    FilteredConformation insertFiltered(FilteredConformation conformation);

    int insertAllSelected(Collection<SelectedConformation> conformations);

    int insertAllFiltered(Collection<FilteredConformation> conformations);

    SelectedConformation deleteSelected(String id);

    FilteredConformation deleteFiltered(String id);

    List<SelectedConformation> findAllSelected();

    List<SelectedConformation> findAllSelected(Integer pageSize, List<String> properties);

    List<FilteredConformation> findAllFiltered();

    List<SelectedConformation> findSelectedByNumberOfAtoms(int n);

    List<SelectedConformation> findAllSelectedGlobalMinima();

    List<FilteredConformation> findFilteredByNumberOfAtoms(int n);
}
