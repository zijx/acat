package edu.ssau.acat.service.prediction.impl;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.conformation.IterativeConformationFilter;
import edu.ssau.acat.core.api.generator.ClusterConformationGenerator;
import edu.ssau.acat.core.impl.filter.conformation.bulk.Dregs;
import edu.ssau.acat.core.impl.filter.pipeline.FilterPipeline;
import edu.ssau.acat.core.impl.generator.MorseClusterConformationGenerator;
import edu.ssau.acat.model.conformation.FilteredConformation;
import edu.ssau.acat.model.conformation.SelectedConformation;
import edu.ssau.acat.model.mapper.ConformationMapper;
import edu.ssau.acat.service.persistence.ConformationPersistenceService;
import edu.ssau.acat.service.persistence.LineagePersistenceService;
import edu.ssau.acat.service.prediction.ClusterPredictionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// This actually fails.. It is a demo only.
@Deprecated
public class ConcurrentClusterPredictionServiceImpl implements ClusterPredictionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConcurrentClusterPredictionServiceImpl.class);

    public static final int BIG_CHILDREN_NUMBER = 1000;
    public static final double DELTA_TOLERANCE = 0.0000001;

    private final FilterPipeline filterPipeline;
    private final ClusterConformationGenerator conformationGenerator;

    private final ConformationPersistenceService conformationService;
    private final LineagePersistenceService lineageService;

    //private ExecutorService executorService;

    @Autowired
    public ConcurrentClusterPredictionServiceImpl(FilterPipeline filterPipeline, ConformationPersistenceService
            conformationService,
            LineagePersistenceService lineageService) {
        this.filterPipeline = filterPipeline;
        this.conformationGenerator = new MorseClusterConformationGenerator(filterPipeline);
        this.conformationService = conformationService;
        this.lineageService = lineageService;
    }

    @Override
    public void start(int startingDimension, int targetDimension, List<ClusterConformation> rootConformations) {
        List<SelectedConformation> conformations = conformationService.findSelectedByNumberOfAtoms(startingDimension);
        if (conformations.isEmpty()) {
            // If conformations of startingDimension have not been persisted, start fom the 'root' nodes
            startFromNodes(rootConformations, targetDimension);
        } else {
            startFromNodes(ConformationMapper.mapSelectedToClusterConformations(conformations), targetDimension);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void startFromNodes(Collection<? extends ClusterConformation> children, int targetDimension) {
        int currentAtomsNumber = children.iterator().next().getAtomsNumber();
        if (currentAtomsNumber < targetDimension) {
            SortedSet<ClusterConformation> grandChildren = new TreeSet<>();
            if (children.size() >= BIG_CHILDREN_NUMBER) {
                LOGGER.debug("Launching parallel streaming.");
                List<SortedSet> childSets = children.parallelStream()
                        .map(child -> conformationGenerator.generateChildConformations(child))
                        .collect(Collectors.toList());
                childSets.forEach((c) -> grandChildren.addAll(c));
            } else {
                for (ClusterConformation child : children) {
                    grandChildren.addAll(conformationGenerator.generateChildConformations(child));
                }
            }
            // Passes all grandchildren through iterative and bulk filters
            passThroughBulkFilters(passThroughIterativeFilters(grandChildren));
            // Sets GO deltas to simplify further statistics processing
            setGODeltas(grandChildren);
            // Persists GO lineage
            persistsGOLineage(grandChildren);
            // Persists all grandchildren
            persistSelectedConformations(grandChildren);
            startFromNodes(grandChildren, targetDimension);
        }
    }

    private SortedSet<ClusterConformation> passThroughIterativeFilters(SortedSet<ClusterConformation> grandChildren) {
        SortedSet<ClusterConformation> filteredGrandChildren = new TreeSet<ClusterConformation>();
        grandChildren.stream().filter(grandChild -> !filterPipeline
                .applyIterativeConformationFilters(grandChild, filteredGrandChildren))
                .forEach(this::persistConformationExcludedByIterativeFilters);
        LOGGER.info("Applied iterative filters: current fit conformations number {}:", filteredGrandChildren.size());
        return filteredGrandChildren;
    }

    private void passThroughBulkFilters(SortedSet<ClusterConformation> grandChildren) {
        List<Dregs> dregsList = filterPipeline.applyBulkConformationFilters(grandChildren);
        for (Dregs dregsEntry : dregsList) {
            Collection<FilteredConformation> filteredConformations = ConformationMapper.mapClusterToFilteredConformations(
                    dregsEntry.getFilteredConformations(), dregsEntry.getFilter());

            LOGGER.debug("Persisting {} conformations excluded by bulk filter {}", dregsEntry.getFilteredConformations().size(),
                    dregsEntry.getFilter().getFilterName());
            conformationService.insertAllFiltered(filteredConformations);
        }
        LOGGER.info("Applied bulk filters: reduced positions number to {}", grandChildren.size());
    }

    private void persistSelectedConformations(SortedSet<ClusterConformation> grandChildren) {
        conformationService.insertAllSelected(ConformationMapper.mapClusterToSelectedConformations(grandChildren));
    }

    private void persistsGOLineage(SortedSet<ClusterConformation> grandChildren) {
        lineageService.insert(ConformationMapper.mapClusterConformationToLineage(grandChildren.first()));
    }

    private void persistConformationExcludedByIterativeFilters(ClusterConformation commonChildGrandChild) {
        IterativeConformationFilter lastIterativeConformationFilter = filterPipeline.getLastIterativeConformationFilter();
        LOGGER.info("Persisting conformation excluded by iterative filter {}", lastIterativeConformationFilter.getFilterName());
        conformationService.insertFiltered(ConformationMapper.mapClusterToFiltered(commonChildGrandChild,
                lastIterativeConformationFilter));
    }

    /**
     * Utility method to set deltas between each generated child and current GO.
     * @param children
     */
    private void setGODeltas(SortedSet<ClusterConformation> children) {
        double goPotential = children.first().getPotentialValue();
        for (ClusterConformation child : children) {
            double delta = Math.abs(child.getPotentialValue() - goPotential);
            if (delta < DELTA_TOLERANCE)
                delta = 0.;
            child.setGoDelta(delta);
        }
    }
}
