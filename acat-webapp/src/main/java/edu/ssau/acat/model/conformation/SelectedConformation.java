package edu.ssau.acat.model.conformation;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "conformations")
public class SelectedConformation extends Conformation implements Comparable<SelectedConformation> {

    private double [] coordinates;

    /**
     * States whether this conformation refers to putative global minimum in Cambridge Cluster Database.
     */
    private boolean isGO;

    /**
     * Delta between this conformation's potential value and GO.
     */
    //private double delta;

    public static Builder getBuilder() {
        return new Builder();
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public boolean isGO() {
        return isGO;
    }

    public void setIsGO(boolean isGO) {
        this.isGO = isGO;
    }

    @Override
    public int compareTo(SelectedConformation o) {
        if (this.potential < o.getPotential()) {
            return -1;
        }
        if (this.potential > o.getPotential()) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SelectedConformation that = (SelectedConformation) o;

        if (Double.compare(that.potential, potential) != 0) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(potential);
        return (int) (temp ^ (temp >>> 32));
    }


    public static class Builder {

        private SelectedConformation built;

        private Builder() {
            this.built = new SelectedConformation();
        }

        public Builder withCoordinates(double[] coordinates) {
            built.coordinates = coordinates;
            return this;
        }

        public Builder withNumberOfAtoms(int numberOfAtoms) {
            built.n = numberOfAtoms;
            return this;
        }

        public Builder withPotential(double potential) {
            built.potential = potential;
            return this;
        }

        public Builder withParentPotential(double potential) {
            built.parent = potential;
            return this;
        }

        public Builder withRelativeNNDistance(double distance) {
            built.nnd = distance;
            return this;
        }

        public Builder withRho(int rho) {
            built.rho = rho;
            return this;
        }

        public Builder isGlobalMinimum() {
            built.isGO = true;
            return this;
        }

        public SelectedConformation build() {
            return built;
        }
    }
}
