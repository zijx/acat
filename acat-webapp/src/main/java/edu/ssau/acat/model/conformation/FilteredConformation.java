package edu.ssau.acat.model.conformation;

import java.util.Properties;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "candidates")
public class FilteredConformation extends Conformation {

    /**
     * Filter name.
     */
    private String filter;

    /**
     * Filter configuration.
     */
    private Properties configuration;

    public Properties getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Properties configuration) {
        this.configuration = configuration;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public static class Builder {

        private FilteredConformation built;

        private Builder() {
            this.built = new FilteredConformation();
        }

        public Builder withNumberOfAtoms(int numberOfAtoms) {
            built.n = numberOfAtoms;
            return this;
        }

        public Builder withPotential(double potential) {
            built.potential = potential;
            return this;
        }

        public Builder withRelativeNNDistance(double distance) {
            built.nnd = distance;
            return this;
        }

        public Builder withParentPotential(double potential) {
            built.parent = potential;
            return this;
        }

        public Builder withRho(int rho) {
            built.rho = rho;
            return this;
        }

        public Builder withFilter(String filterName) {
            built.filter = filterName;
            return this;
        }

        public Builder withFilterConfiguration(Properties configuration) {
            built.configuration = configuration;
            return this;
        }

        public FilteredConformation build() {
            return built;
        }
    }
}
