package edu.ssau.acat.model.lineage;

import org.springframework.data.annotation.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Lineage {

    @Id
    private String id;

    /**
     * Number of atoms.
     */
    private int n;

    /**
     * All the ancestors in the lineage chain.
     */
    private double [] ancestors;

    /**
     * Deltas between ancestors' potential values and the corresponding GOs.
     */
    private double [] deltas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public double[] getAncestors() {
        return ancestors;
    }

    public void setAncestors(double[] ancestors) {
        this.ancestors = ancestors;
    }

    public double[] getDeltas() {
        return deltas;
    }

    public void setDeltas(double[] deltas) {
        this.deltas = deltas;
    }

    @Override public String toString() {
        return new ToStringBuilder(this)
                .append("n", n)
                .toString();
    }

    public static class Builder {

        private Lineage built;

        private Builder() {
            this.built = new Lineage();
        }

        public Builder withNumberOfAtoms(int numberOfAtoms) {
            built.n = numberOfAtoms;
            return this;
        }

        public Builder withAncestors(double[] ancestors) {
            built.ancestors = ancestors;
            return this;
        }

        public Builder withGODeltas(double[] deltas) {
            built.deltas = deltas;
            return this;
        }

        public Lineage build() {
            return built;
        }
    }
}
