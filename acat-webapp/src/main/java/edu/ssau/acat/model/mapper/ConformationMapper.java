package edu.ssau.acat.model.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.Filter;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;
import edu.ssau.acat.model.conformation.FilteredConformation;
import edu.ssau.acat.model.conformation.SelectedConformation;
import edu.ssau.acat.model.lineage.Lineage;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public final class ConformationMapper {

    private ConformationMapper() {
    }

    // todo: ideally we should avoid hard-coding of concrete impl here
    public static Collection<? extends ClusterConformation> mapSelectedToClusterConformations(Collection<SelectedConformation>
            sourceConformations) {
        List<MorseClusterConformation> targetConformations = sourceConformations.stream().map(sourceConformation -> new
                MorseClusterConformation(Vector3DHelper.getVector3DList(sourceConformation
                .getCoordinates()))).collect(Collectors.toList());
        return targetConformations;
    }

    public static Collection<FilteredConformation> mapClusterToFilteredConformations(Collection<ClusterConformation>
            sourceConformations, Filter filter) {
        List<FilteredConformation> targetConformations = sourceConformations.stream().map(sourceConformation ->
                mapClusterToFiltered(sourceConformation, filter)).collect(Collectors.toList());
        return targetConformations;
    }

    public static SortedSet<SelectedConformation> mapClusterToSelectedConformations(
            SortedSet<? extends ClusterConformation> sourceConformations) {
        SortedSet<SelectedConformation> targetConformations = new TreeSet<>();
        if (!sourceConformations.isEmpty()) {
            Iterator<? extends ClusterConformation> iterator = sourceConformations.iterator();
            ClusterConformation go = sourceConformations.iterator().next();
            checkNotNull(go, "GO conformation can't be null");
            targetConformations.add(mapClusterGOToSelected(go));
            while (iterator.hasNext()) {
                targetConformations.add(mapClusterToSelected(iterator.next()));
            }
        }
        return targetConformations;
    }

    public static Lineage mapClusterConformationToLineage(final ClusterConformation source) {
        List<Double> ancestors = new ArrayList<>();
        List<Double> deltas = new ArrayList<>();
        // Each lineage chain starts from the 'youngest' conformation whose lineage is being mapped
        ancestors.add(source.getPotentialValue());
        deltas.add(source.getGoDelta());
        ClusterConformation parent = source.getParent();
        while (parent != null) {
            ancestors.add(parent.getPotentialValue());
            deltas.add(parent.getGoDelta());
            parent = parent.getParent();
        }
        // Converts to double array
        double[] ancestorArray = new double[ancestors.size()];
        double[] deltaArray = new double[ancestors.size()];
        for (int i = 0; i < ancestorArray.length; i++) {
            ancestorArray[i] = ancestors.get(i);
            deltaArray[i] = deltas.get(i);
        }

        return Lineage.getBuilder()
                .withNumberOfAtoms(source.getAtomsNumber())
                .withAncestors(ancestorArray)
                .withGODeltas(deltaArray)
                .build();
    }

    public static SelectedConformation mapClusterToSelected(final ClusterConformation source) {
        validateConformation(source);
        double nnDistance = getNNDistance(source);
        return getBuilderForDefaultConformation(source)
                .withRelativeNNDistance(nnDistance)
                .build();
    }

    public static SelectedConformation mapClusterGOToSelected(final ClusterConformation source) {
        validateConformation(source);
        double nnDistance = getNNDistance(source);
        return getBuilderForDefaultConformation(source)
                .withRelativeNNDistance(nnDistance)
                .isGlobalMinimum()
                .build();
    }

    public static FilteredConformation mapClusterToFiltered(final ClusterConformation source, Filter filter) {
        validateConformation(source);
        double nnDistance = getNNDistance(source);
        return FilteredConformation.getBuilder()
                .withNumberOfAtoms(source.getAtomsNumber())
                .withPotential(source.getPotentialValue())
                .withParentPotential(source.getParent().getPotentialValue())
                .withRho((int) source.getPotentialFunction().getParameters()[0])
                .withRelativeNNDistance(nnDistance)
                .withFilter(filter.getFilterName())
                .withFilterConfiguration(filter.getConfiguration())
                .build();
    }

    private static double getNNDistance(ClusterConformation source) {
        double nnDistance = 0.;
        if (source.getNearestNeighbourParameters() != null)
            nnDistance = source.getNearestNeighbourParameters().getNNRelativeDistance();
        return nnDistance;
    }

    private static SelectedConformation.Builder getBuilderForDefaultConformation(final ClusterConformation source) {
        validateConformation(source);
        return SelectedConformation.getBuilder()
                .withNumberOfAtoms(source.getAtomsNumber())
                .withPotential(source.getPotentialValue())
                .withParentPotential(source.getParent().getPotentialValue())
                .withRho((int) source.getPotentialFunction().getParameters()[0])
                .withCoordinates(Vector3DHelper.getDoubleArrayFromVector3DList(
                        source.getRelativeAtomicCoordinates()));
    }

    private static void validateConformation(final ClusterConformation source) {
        checkNotNull(source.getParent());
        checkNotNull(source.getPotentialFunction());
        checkArgument(source.getPotentialFunction().getParameters().length > 0);
    }
}
