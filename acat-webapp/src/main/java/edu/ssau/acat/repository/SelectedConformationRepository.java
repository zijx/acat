package edu.ssau.acat.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import edu.ssau.acat.model.conformation.SelectedConformation;

public interface SelectedConformationRepository extends MongoRepository<SelectedConformation, String> {

    List<SelectedConformation> findByN(int n);

    List<SelectedConformation> findByIsGOIsTrue();

    //todo add other query methods
}
