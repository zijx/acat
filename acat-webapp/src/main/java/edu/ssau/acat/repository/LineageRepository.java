package edu.ssau.acat.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import edu.ssau.acat.model.lineage.Lineage;

public interface LineageRepository extends MongoRepository<Lineage, String> {

    Lineage findByN(int n);

    //todo add other query methods
}
