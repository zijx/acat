package edu.ssau.acat.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import edu.ssau.acat.model.conformation.FilteredConformation;

public interface FilteredConformationRepository extends MongoRepository<FilteredConformation, String> {

    List<FilteredConformation> findByN(int n);

    //todo add other query methods
}
