package edu.ssau.acat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.ssau.acat.core.impl.filter.conformation.bulk.ConformationFilterByPotentialVariance;
import edu.ssau.acat.core.impl.filter.conformation.iterative.ConformationFilterByContributionArray;
import edu.ssau.acat.core.impl.filter.pipeline.FilterPipeline;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByCutOff;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByPotentialVariance;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByRelativePotentialVariance;
import edu.ssau.acat.core.impl.filter.position.iterative.PositionFilterByDistanceVector;

@Configuration
public class AppConfig {

    @Bean
    public FilterPipeline filterPipeline() {
        FilterPipeline filterPipeline = new FilterPipeline();

        // Registers position filters
        filterPipeline.registerFilter(new PositionFilterByDistanceVector());
        filterPipeline.registerFilter(new PositionFilterByPotentialVariance());
        filterPipeline.registerFilter(new PositionFilterByRelativePotentialVariance());
        filterPipeline.registerFilter(new PositionFilterByCutOff());

        // Registers conformation filters
        filterPipeline.registerFilter(new ConformationFilterByContributionArray());
        //filterPipeline.registerFilter(new ConformationFilterByNearestNeighbours());
        filterPipeline.registerFilter(new ConformationFilterByPotentialVariance());

        return filterPipeline;
    }
}
