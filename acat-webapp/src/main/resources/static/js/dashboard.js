var db = new EmbeddedDashboard ();

var chart = new ChartComponent();
chart.setDimensions (6, 6);
chart.setCaption("First Chart");
chart.setLabels (["A", "B", "C"]);
chart.addSeries ("a", "a", [1355, 1916, 1150]);
chart.addSeries ("b", "b", [1513, 976, 1321]);
db.addComponent (chart);

var chart2 = new ChartComponent();
chart2.setDimensions (6, 6);
chart2.setCaption("First Chart");
chart2.setLabels (["D", "F", "G"]);
chart2.addSeries ("d", "d", [1255, 1116, 1750]);
chart2.addSeries ("f", "f", [1313, 1976, 1121]);
db.addComponent (chart2);

var lineageChart = new ChartComponent();
lineageChart.setCaption("GO pathways");
//lineageChart.setDimensions (6, 6);
lineageChart.setLabels (["26","25","24","23","22","21","20",
    "19","18","17","16","15","14","13","12","11","10","9","8","7"]);
// Locks chart until the data is fetched
lineageChart.lock();
db.addComponent (lineageChart);

$.get("/lineage/26", function (data) {
    // This function is executed when the ajax request is successful.

    lineageChart.addSeries ( data['deltas'], {
        seriesDisplayType: "line"
    });

    // Don't forget to call unlock or the data won't be displayed
    lineageChart.unlock ();
});

/*    chart.onItemClick (function (params) {
 lineageChart.updateSeries ("series_1", [3, 5, 2]);
 });*/

db.embedTo("dashboard_target");

