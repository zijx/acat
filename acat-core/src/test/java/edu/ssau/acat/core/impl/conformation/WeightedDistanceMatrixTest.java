package edu.ssau.acat.core.impl.conformation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WeightedDistanceMatrixTest {

    public static final int DEFAULT_DIMENSION = 5;

    private static WeightedDistanceMatrix distanceMatrix = new WeightedDistanceMatrix(DEFAULT_DIMENSION);

    @Test
    public void testGetDistanceValueAt_ValidIndicesGiven_ShouldReturnDistanceValue() {
        for (int i = 0, k = 0; i < DEFAULT_DIMENSION; i++) {
            for (int j = 0; j < DEFAULT_DIMENSION; j++, k++) {
                distanceMatrix.putDistanceValueAt(i, j, k);
            }
        }

        assertEquals(6., distanceMatrix.getDistanceValueAt(1, 1), 0);
        assertEquals(17., distanceMatrix.getDistanceValueAt(3, 2), 0);
        assertEquals(24., distanceMatrix.getDistanceValueAt(4, 4), 0);
    }
}
