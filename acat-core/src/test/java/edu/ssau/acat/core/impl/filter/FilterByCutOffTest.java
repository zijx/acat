package edu.ssau.acat.core.impl.filter;

import java.util.SortedSet;
import java.util.TreeSet;

import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByCutOff;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FilterByCutOffTest {

    public static final int CUT_OFF = 3;
    public static final int ORIGINAL_SIZE = 5;
    private PositionFilterByCutOff filterByCutOff;

    @Before
    public void setUp() throws Exception {
        filterByCutOff = new PositionFilterByCutOff(CUT_OFF);
    }

    @Test
    public void testApplyFilter_SetGiven_ShouldRetainCutOffElements() {
        SortedSet<WeightedPosition> positions = new TreeSet<WeightedPosition>();
        for (int i = 0; i < ORIGINAL_SIZE; i++) {
            positions.add(new WeightedPosition(new Vector3D(1, 2, 3), i));
        }
        filterByCutOff.applyFilter(positions);

        assertEquals(CUT_OFF, positions.size());
    }
}
