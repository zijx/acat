package edu.ssau.acat.core.impl.generator;

import java.util.List;

import edu.ssau.acat.core.impl.conformation.dto.MorseConformations;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AtomicTripletsExtractorTest {

    public static final int TRIPLES_NUMBER_FOR_7A = 35;

    private AtomicTripletsExtractor triplesExtractor;

    @Before
    public void setUp() {
        triplesExtractor = new AtomicTripletsExtractor();
    }

    @Test
    public void testExtractTriples_WeightedDistanceMatrixGiven_ReturnTripleList() {
        MorseClusterConformation conformation = MorseConformations.createMorseConformation7A();
        //WeightedDistanceMatrix matrixFor7A = conformation.getWeightedDistanceMatrix();
        List<AtomicTriplet> triples = triplesExtractor.extractTriplets(conformation);

        assertEquals(TRIPLES_NUMBER_FOR_7A, triples.size());
    }
}
