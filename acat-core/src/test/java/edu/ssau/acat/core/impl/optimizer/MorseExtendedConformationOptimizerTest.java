package edu.ssau.acat.core.impl.optimizer;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Before;

public class MorseExtendedConformationOptimizerTest {

    public static final double DELTA = 0.000001;

    public static final Vector3D CANDIDATE_POSITION_FOR_137_OF_7A = new Vector3D(0.8247361, -0.557689189, -0.825611683);
    public static final Vector3D OPTIMIZED_POSITION_FOR_137_OF_7A = new Vector3D(0.820463976, -0.55458528, -0.824227263);

    public static final double ORIGINAL_POTENTIAL_VALUE_FOR_137_OF_7A = -19.32459972;
    public static final double OPTIMIZED_POTENTIAL_VALUE_FOR_137_OF_7A = -19.32615896;

    private BasicPositionOptimizer optimizer;

    @Before
    public void setUp() throws Exception {
        optimizer = new BasicPositionOptimizer();
    }

/*    @Test
    public void testOptimizePositionForConformation_PositionFor137And7AConformationGiven_ShouldReturnOptimizedPosition() {
        MorseClusterConformation morseConformation7A = MorseConformations.createMorseConformation7A();
        Vector3D optimizedPosition = optimizer.optimize(CANDIDATE_POSITION_FOR_137_OF_7A,
                morseConformation7A);
        double morse7APotentialValue = morseConformation7A.getPotentialValue();
        PairPotentialFunction morseFunction = morseConformation7A.getPotentialFunction();

        assertEquals(ORIGINAL_POTENTIAL_VALUE_FOR_137_OF_7A, morseConformation7A
                .evaluateContribution(CANDIDATE_POSITION_FOR_137_OF_7A) + morse7APotentialValue, DELTA);
        assertEquals(OPTIMIZED_POSITION_FOR_137_OF_7A, optimizedPosition);
        assertEquals(OPTIMIZED_POTENTIAL_VALUE_FOR_137_OF_7A, morseConformation7A
                .evaluateContribution(optimizedPosition) + morse7APotentialValue, DELTA);
    }*/
}
