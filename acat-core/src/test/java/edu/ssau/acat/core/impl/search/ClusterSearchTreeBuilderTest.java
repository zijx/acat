package edu.ssau.acat.core.impl.search;

import java.util.Set;
import java.util.TreeSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.impl.conformation.dto.MorseConformations;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.filter.conformation.iterative.ConformationFilterByContributionArray;
import edu.ssau.acat.core.impl.filter.conformation.bulk.ConformationFilterByNearestNeighbours;
import edu.ssau.acat.core.impl.filter.conformation.bulk.ConformationFilterByPotentialVariance;
import edu.ssau.acat.core.impl.filter.pipeline.FilterPipeline;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByCutOff;
import edu.ssau.acat.core.impl.filter.position.iterative.PositionFilterByDistanceVector;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByPotentialVariance;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByRelativePotentialVariance;
import edu.ssau.acat.core.impl.generator.MorseClusterConformationGenerator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class ClusterSearchTreeBuilderTest {

    public static final int TARGET_DIMENSION_FOR_7A = 7;
    public static final int TARGET_DIMENSION_FOR_9B = 9;

    public static final double DELTA = 0.000001;
    public static final int CUT_OFF = 3;

    private ClusterSearchTreeBuilder searchTreeBuilder;

    @Before
    public void setUp() {
        FilterPipeline filterPipeline = new FilterPipeline();
        // Registers position filters
        filterPipeline.registerFilter(new PositionFilterByDistanceVector());
        filterPipeline.registerFilter(new PositionFilterByPotentialVariance());
        filterPipeline.registerFilter(new PositionFilterByRelativePotentialVariance());
        filterPipeline.registerFilter(new PositionFilterByCutOff(CUT_OFF));
        // Registers conformation filters
        filterPipeline.registerFilter(new ConformationFilterByContributionArray());
        filterPipeline.registerFilter(new ConformationFilterByPotentialVariance());
        //filterPipeline.registerFilter(new ConformationFilterByNearestNeighbours());
        searchTreeBuilder = new ClusterSearchTreeBuilder(filterPipeline, new MorseClusterConformationGenerator(filterPipeline));
    }

/*    @Ignore("Fails")
    @Test
    public void testStartSearchFromRoot_MorseCluster5AGiven_ShouldFind7A() {
        Set<ClusterConformation> clusterConformations = searchTreeBuilder.startSearchFromRoot(MorseConformations
                .createMorseConformation5A(), TARGET_DIMENSION_FOR_7A);

        assertThat(clusterConformations).extracting("getPotentialValue").contains(MorseConformations.POTENTIAL_FOR_7A);
    }

    @Test
    public void testStartSearchFromRoot_MorseCluster7AGiven_ShouldFind9B() {
        MorseClusterConformation morseCluster7A = MorseConformations.createMorseConformation7A();
        Set<ClusterConformation> clusterConformations = searchTreeBuilder.startSearchFromRoot(morseCluster7A, TARGET_DIMENSION_FOR_9B);

        assertThat(clusterConformations).extracting("potentialValue").contains(MorseConformations.POTENTIAL_FOR_9B);
    }*/

    @Ignore
    @Test
    public void testStartSearchFromRoot_MorseCluster7AAnd7XBGiven_ShouldFind3DirectNodes() {
        MorseClusterConformation morseCluster7A = MorseConformations.createMorseConformation7A();
        MorseClusterConformation morseCluster7XB = MorseConformations.createMorseConformation7XB();

        Set<ClusterConformation> conformations = new TreeSet<>();
        conformations.add(morseCluster7A);
        conformations.add(morseCluster7XB);

        searchTreeBuilder.startSearchFromNodes(conformations, 8);
        //assertEquals(3, generatedNodes);
    }

    @Ignore
    @Test
    public void testStartSearchFromRoot_MorseCluster7AAnd7XBGiven_ShouldFindNNodesWithNAtoms() {
        MorseClusterConformation morseCluster7A = MorseConformations.createMorseConformation7A();
        MorseClusterConformation morseCluster7XB = MorseConformations.createMorseConformation7XB();

        Set<ClusterConformation> conformations = new TreeSet<>();
        conformations.add(morseCluster7A);
        conformations.add(morseCluster7XB);

        searchTreeBuilder.startSearchFromNodes(conformations, 17);
    }

    @Ignore
    @Test
    public void testStartSearchFromRoot_MorseCluster9ABGiven_ShouldFindNNodesWithNAtoms() {
        MorseClusterConformation morseCluster19A = MorseConformations.createMorseConformation19A();

        Set<ClusterConformation> conformations = new TreeSet<>();
        conformations.add(morseCluster19A);

        searchTreeBuilder.startSearchFromNodes(conformations, 20);
    }

    @Ignore
    @Test
    public void testStartSearchFromRoot_MorseCluster3_ShouldFindNNodesWithNAtoms() {
        MorseClusterConformation morseCluster3 = MorseConformations.createMorseConformation3();

        Set<ClusterConformation> conformations = new TreeSet<>();
        conformations.add(morseCluster3);

        searchTreeBuilder.startSearchFromNodes(conformations, 18);
    }
}
