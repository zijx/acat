package edu.ssau.acat.core.impl.utilities.math3;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Vector3DHelperTest {

    private static final double[] DOUBLES = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    @Test
    public void testGetVector3DList_ArrayWithLengthDivisibleBy3_ShouldReturnVector3DList() {
        List<Vector3D> vector3DList = Vector3DHelper.getVector3DList(DOUBLES);

        assertEquals(DOUBLES.length / 3, vector3DList.size());
        assertEquals(new Vector3D(1, 2, 3), vector3DList.get(0));
        assertEquals(new Vector3D(4, 5 ,6), vector3DList.get(1));
        assertEquals(new Vector3D(7, 8, 9), vector3DList.get(2));
    }

    @Test
    public void testGetDoubleArrayFromVector3DList_Vector3DListGiven_ShouldReturnDoubleArray() {
        List<Vector3D> vector3DList = Vector3DHelper.getVector3DList(DOUBLES);
        double[] array = Vector3DHelper.getDoubleArrayFromVector3DList(vector3DList);

        assertEquals(DOUBLES.length, array.length);
        assertArrayEquals(DOUBLES, array, 0);
    }
}
