package edu.ssau.acat.core.impl.potential;

import edu.ssau.acat.core.impl.potential.morse.MorsePairPotentialFunction;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MorsePairPotentialFunctionTest {

    public static final int DEFAULT_DIMENSION = 7 * 3;

    public static final double DELTA = 0.000001;

    public static final double[] EXPANDED_7A_COORDINATES = {0.9104573, -1.31E-11, -3.41E-10, 0.3208315, 0.8115503, 1.30E-09,
            0.3208315, -0.8115503, -6.19E-10, -0.6332031, 0.5015657, 2.04E-09, -0.6332031, -0.5015657, 8.53E-10, 0.05714286,
            3.28E-11, 0.5097267, 0.05714286, 2.02E-09, -0.5097267, 0.820463976, -0.55458528, -0.824227263
    };

    public static final double[] EXPANDED_7A_GRADIENT = {-0.016589537, -0.102186261, -0.151869718,
            -0.062515493, 0.170927506, 0.103127572, 0.092059562, 0.047351221, -0.15186964,
            -0.025062544, 0.018213599, 0.014208928, -0.181886138, 0.006627171, 0.10312766,
            -0.122186608, 0.088779523, 0.21353992, 0.316181594, -0.229713392, -0.130265904, -8.35E-07, 6.32E-07, 1.18E-06
    };

    private MorsePairPotentialFunction morsePairPotentialFunction;

    @Before
    public void setUp() {
        morsePairPotentialFunction = new MorsePairPotentialFunction(DEFAULT_DIMENSION);
    }

    @Test
    public void testValueAt_DoubleArrayGiven_ShouldReturnMorsePotentialValue() {
        double morsePotential = morsePairPotentialFunction.valueAt(EXPANDED_7A_COORDINATES);

        assertEquals(-19.32615896, morsePotential, DELTA);
    }

    @Test
    public void testDerivativeAt_DoubleArrayGiven_ShouldReturnGradientVector() {
        double[] gradientVector = morsePairPotentialFunction.derivativeAt(EXPANDED_7A_COORDINATES);

        assertArrayEquals(EXPANDED_7A_GRADIENT, gradientVector, DELTA);
    }
}
