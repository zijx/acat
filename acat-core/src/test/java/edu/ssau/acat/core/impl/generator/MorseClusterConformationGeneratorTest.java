package edu.ssau.acat.core.impl.generator;

import java.util.Iterator;
import java.util.Set;

import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import edu.ssau.acat.core.impl.conformation.dto.MorseConformations;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.filter.pipeline.FilterPipeline;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByCutOff;
import edu.ssau.acat.core.impl.filter.position.iterative.PositionFilterByDistanceVector;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByPotentialVariance;
import edu.ssau.acat.core.impl.filter.position.bulk.PositionFilterByRelativePotentialVariance;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MorseClusterConformationGeneratorTest {

    public static final double OPTIMIZED_POTENTIAL_FOR_7A = -16.20758;
    public static final double OPTIMIZED_POTENTIAL_FOR_7XB = -15.56377;

    public static final MorseClusterConformation MORSE_CONFORMATION_7A = MorseConformations.createMorseConformation7A();
    public static final double OPTIMIZED_POTENTIAL_FOR_8A = -19.32742;

    public static final double DELTA = 0.00001;

    private MorseClusterConformationGenerator generator;

    @Before
    public void setUp() throws Exception {
        FilterPipeline filterPipeline = new FilterPipeline();
        filterPipeline.registerFilter(new PositionFilterByDistanceVector());
        filterPipeline.registerFilter(new PositionFilterByPotentialVariance());
        filterPipeline.registerFilter(new PositionFilterByRelativePotentialVariance());
        filterPipeline.registerFilter(new PositionFilterByCutOff(3));
        generator = new MorseClusterConformationGenerator(filterPipeline);
    }

    @Test
    public void testGetWeightedPositions_ParentConformation7AGiven_ShouldReturnSingleCandidate() {
        Set<WeightedPosition> positions = generator.getWeightedPositions(MORSE_CONFORMATION_7A);

        assertEquals(1, positions.size());
    }

    @Ignore
    @Test
    public void testGenerateChildConformations_ParentConformation6AGiven_ShouldReturn2Conformations() {
        MorseClusterConformation morseConformation6a = MorseConformations.createMorseConformation6A();
        Set<MorseClusterConformation> childConformations = generator.generateChildConformations(morseConformation6a);
        Iterator<MorseClusterConformation> iterator = childConformations.iterator();

        assertEquals(2, childConformations.size());
        assertEquals(OPTIMIZED_POTENTIAL_FOR_7A, iterator.next().getPotentialValue(), DELTA);
        assertEquals(OPTIMIZED_POTENTIAL_FOR_7XB, iterator.next().getPotentialValue(), DELTA);
    }

    @Ignore
    @Test
    public void testGenerateChildConformations_ParentConformation7AGiven_ShouldReturnOptimalConformation() {
        MorseClusterConformation morseConformation7a = MORSE_CONFORMATION_7A;
        Set<MorseClusterConformation> childConformations = generator.generateChildConformations(morseConformation7a);
        Iterator<MorseClusterConformation> iterator = childConformations.iterator();

        assertEquals(1, childConformations.size());
        assertEquals(OPTIMIZED_POTENTIAL_FOR_8A, iterator.next().getPotentialValue(), DELTA);
    }

    @Ignore
    @Test
    public void testGenerateChildConformations_ParentConformation28XGiven_ShouldReturn14OptimalConformations() {
        MorseClusterConformation morseConformation28x = MorseConformations.createMorseConformation28X();
        Set<MorseClusterConformation> childConformations = generator.generateChildConformations(morseConformation28x);

        assertEquals(14, childConformations.size());
        //assertEquals(OPTIMIZED_POTENTIAL_FOR_8A, iterator.next().getPotentialValue(), DELTA);
    }
}
