package edu.ssau.acat.core.impl.generator;

import java.util.List;

import edu.ssau.acat.core.impl.conformation.dto.MorseConformations;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class ExtendedAtomicTripletGeneratorTest {

    public static final Vector3D CANDIDATE_POSITION_FOR_137_OF_7A = new Vector3D(0.8247361, -0.557689189, -0.825611683);
    public static final double DELTA = 0.000001;

    @Ignore("This test does not pass because of the precision")
    @Test
    public void testExtendTriplet_AtomicTriplet137From7AGiven_ShouldReturnTwoCoordinates() {
        // Tries 1-3-7 triplet of 7A
        int i = 0;
        int j = 2;
        int k = 6;
        MorseClusterConformation morseConformation7A = MorseConformations.createMorseConformation7A();
        Vector3D first = morseConformation7A.getRelativeAtomicCoordinate(i);
        Vector3D second = morseConformation7A.getRelativeAtomicCoordinate(j);
        Vector3D third = morseConformation7A.getRelativeAtomicCoordinate(k);
        List<Vector3D> positions = ExtendedAtomicTripletGenerator.extendTriplet(new AtomicTriplet(first, second, third));

        assertEquals(2, positions.size());
        assertThat(positions).contains(CANDIDATE_POSITION_FOR_137_OF_7A);
    }
}
