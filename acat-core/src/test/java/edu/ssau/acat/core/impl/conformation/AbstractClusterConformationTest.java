package edu.ssau.acat.core.impl.conformation;

import java.util.Arrays;

import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class AbstractClusterConformationTest {

    public static final double DELTA = 0.000001;

    public static final double[] MORSE_CLUSTER_7A_EXTENDED_WITH_137_CANDIDATE = {0.9104573, -1.31E-11, -3.41E-10, 0.3208315, 0.8115503, 1.30E-09,
            0.3208315, -0.8115503, -6.19E-10, -0.6332031, 0.5015657, 2.04E-09, -0.6332031, -0.5015657, 8.53E-10,
            0.05714286, 3.28E-11, 0.5097267, 0.05714286, 2.02E-09, -0.5097267, 0.820463976, -0.55458528, -0.824227263
    };

    public static final Vector3D CANDIDATE_POSITION_FOR_137_OF_7A = new Vector3D(0.820463976, -0.55458528, -0.824227263);

    public static final double[] SORTED_CONTRIBUTION_ARRAY_FOR_7A = {
            -6.979961805, -6.025156114, -5.090377879, -5.090377856, -4.125785711, -4.125785677, -4.09629353, -3.1185793
    };

    @Test
    public void testGetContributionVector_Extended7AInstantiated_ShouldReturnContributionArray() {
        MorseClusterConformation conformation = new MorseClusterConformation(Vector3DHelper.getVector3DList(
                MORSE_CLUSTER_7A_EXTENDED_WITH_137_CANDIDATE));
        double[] contributionArray = conformation.getContributionArray();
        Arrays.sort(contributionArray);

        assertEquals(conformation.getAtomsNumber(), contributionArray.length);
        assertArrayEquals(SORTED_CONTRIBUTION_ARRAY_FOR_7A, contributionArray, DELTA);
    }


}
