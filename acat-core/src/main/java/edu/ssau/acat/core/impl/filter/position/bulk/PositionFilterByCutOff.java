package edu.ssau.acat.core.impl.filter.position.bulk;

import java.util.Iterator;
import java.util.SortedSet;

import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.position.BulkPositionFilter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PositionFilterByCutOff extends AbstractFilter implements BulkPositionFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionFilterByCutOff.class);

    public static final int CUT_OFF_VALUE = 3;

    public int cutOff;

    public PositionFilterByCutOff() {
        this.cutOff = CUT_OFF_VALUE;
        setConfiguration();
    }

    public PositionFilterByCutOff(int cutOff) {
        this.cutOff = cutOff;
        setConfiguration();
    }

    @Override
    public boolean applyFilter(SortedSet<WeightedPosition> in) {
        Iterator<WeightedPosition> positionIterator = in.iterator();
        int index = 0;
        while (positionIterator.hasNext()) {
            if (index >= cutOff) {
                positionIterator.remove();
            }
            positionIterator.next();
            index++;
            //in.tailSet(positionIterator.next()).clear();
        }
        LOGGER.debug("Bulk filter {} filtered out {} candidates", this.getClass().getSimpleName(), cutOff);
        return in.size() > cutOff;
    }
}
