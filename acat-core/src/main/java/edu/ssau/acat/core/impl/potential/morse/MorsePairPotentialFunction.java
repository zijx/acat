package edu.ssau.acat.core.impl.potential.morse;

import java.util.List;

import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import edu.ssau.acat.core.impl.problem.ClusterConformationProblem;
import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Represents Morse pair potential function.
 */
public class MorsePairPotentialFunction implements PairPotentialFunction {

    // Choosing rho = 6 in Morse optimization generates problems whose difficulty is very similar to that of LJ
    // optimization and whose global optima are almost indistinguishable from those of LJ clusters of the same size.
    public static final int DEFAULT_RHO_PARAMETER = 6;
    //public static final int DEFAULT_RHO_PARAMETER = 14;
    public static final int SPACE_DIMENSION = ClusterConformationProblem.SPACE_DIMENSION;

    /**
     * The domain dimension: number of atoms * {@code SPACE_DIMENSION}.
     */
    private int dimension;

    /**
     * Rho is a positive parameter. When rho increases, it has been both experimentally and numerically confirmed that the
     * number of local optima, corresponding to semi stable conformations, increases exponentially.
     */
    private int rhoParameter;

    public MorsePairPotentialFunction(int dimension, int rhoParameter) {
        this.dimension = dimension;
        this.rhoParameter = rhoParameter;
    }

    public MorsePairPotentialFunction(int dimension) {
        this(dimension, DEFAULT_RHO_PARAMETER);
    }

    /**
     * Returns contribution M(distance,rho) to Morse potential energy value made by a pair of atoms.
     * @param distance distance between a given pair of atoms
     * @param rho rho parameter
     * @return contribution to Morse potential by a pair of atoms
     */
    public static double pairValueAt(double distance, double rho) {
        double expFactor = Math.exp(rho * (1 - distance));
        return expFactor * (expFactor - 2);
    }

    public static double derivativeOfPotentialWRTDistance(double distance, double rho) {
        double expFactor1 = Math.exp(rho * (1 - distance));
        double expFactor2 = Math.exp(2 * rho * (1 - distance));
        return 2 * rho * (expFactor1 - expFactor2);
    }

    public static Vector3D derivativeOfDistanceWRTXYZ(double distance, Vector3D ithElement, Vector3D jthElement) {
        return ithElement.subtract(jthElement).scalarMultiply(1 / distance);
    }

    public int getRhoParameter() {
        return rhoParameter;
    }

    @Override
    public double valueAt(List<Vector3D> argumentVector) {
        double distance;
        Vector3D ithElement, jthElement;
        double potentialEnergyValue = 0;
        int vectorSize = argumentVector.size();
        for (int i = 0; i < vectorSize; i++) {
            ithElement = argumentVector.get(i);
            for (int j = i + 1; j < vectorSize; j++) {
                jthElement = argumentVector.get(j);
                distance = Vector3D.distance(ithElement, jthElement);
                potentialEnergyValue += pairValueAt(distance, rhoParameter);
            }
        }
        return potentialEnergyValue;
    }

    /**
     * todo document.
     * @param argumentVector
     * @return
     */
    @Override
    public double[] derivativeAt(List<Vector3D> argumentVector) {
        double distance;
        double derivativeOfPotential;
        Vector3D gradientElement;
        Vector3D derivativeOfDistance;
        Vector3D ithElement, jthElement;
        int argumentVectorSize = argumentVector.size();
        double [] gradientVector = new double[argumentVectorSize * SPACE_DIMENSION];
        for (int i = 0; i < argumentVectorSize; i++) {
            ithElement = argumentVector.get(i);
            for (int j = 0; j < argumentVectorSize; j++) {
                if (j != i) {
                    jthElement = argumentVector.get(j);
                    distance = Vector3D.distance(ithElement, jthElement);
                    derivativeOfPotential = derivativeOfPotentialWRTDistance(distance, rhoParameter);
                    derivativeOfDistance = derivativeOfDistanceWRTXYZ(distance, ithElement, jthElement);
                    gradientElement = derivativeOfDistance.scalarMultiply(derivativeOfPotential);
                    gradientVector[SPACE_DIMENSION * i] += gradientElement.getX();
                    gradientVector[SPACE_DIMENSION * i + 1] += gradientElement.getY();
                    gradientVector[SPACE_DIMENSION * i + 2] += gradientElement.getZ();
                }
            }
        }
        return gradientVector;
    }

    public void setRhoParameter(int rhoParameter) {
        this.rhoParameter = rhoParameter;
    }

    @Override
    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    @Override
    public int domainDimension() {
        return dimension;
    }

    @Override
    public double valueAt(double[] arguments) {
        return valueAt(Vector3DHelper.getVector3DList(arguments));
    }

    @Override
    public double pairValueAt(double distance) {
        return pairValueAt(distance, rhoParameter);
    }

    @Override
    public double[] derivativeAt(double[] arguments) {
        return derivativeAt(Vector3DHelper.getVector3DList(arguments));
    }

    @Override
    public double[] getParameters() {
        return new double[]{rhoParameter};
    }
}
