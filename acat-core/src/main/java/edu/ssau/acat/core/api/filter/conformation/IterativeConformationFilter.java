package edu.ssau.acat.core.api.filter.conformation;

import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.Filter;

/**
 * Filters out candidate conformations iteratively - one new candidate at a time.
 */
public interface IterativeConformationFilter extends Filter {

    /**
     * Returns {@code true} if {@code in} passes the filter and should be added to {@code out},
     * returns {@code false} otherwise.
     * @param in
     * @param out
     * @return {@code true} if {@code in} candidate passes the filter, {@code false} otherwise
     */
    boolean applyFilter(ClusterConformation in, SortedSet<ClusterConformation> out);

}
