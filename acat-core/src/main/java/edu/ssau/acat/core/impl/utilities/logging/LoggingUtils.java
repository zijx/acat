package edu.ssau.acat.core.impl.utilities.logging;

public final class LoggingUtils {

    public static final String STRING_DIVIDER =
            "--------------------------------------------------------------------------------------------------------";
    public static final String WIDER_STRING_DIVIDER =
            "========================================================================================================";


    private LoggingUtils() {
    }
}
