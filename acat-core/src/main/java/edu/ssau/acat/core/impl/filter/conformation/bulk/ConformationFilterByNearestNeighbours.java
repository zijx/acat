package edu.ssau.acat.core.impl.filter.conformation.bulk;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.conformation.BulkConformationFilter;
import edu.ssau.acat.core.impl.conformation.nn.NearestNeighbourAnalyzer;
import edu.ssau.acat.core.impl.conformation.nn.NearestNeighbourParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConformationFilterByNearestNeighbours extends AbstractFilter implements BulkConformationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConformationFilterByNearestNeighbours.class);

    private static final int SMALL_ATOMS_NUMBER = 12;
    private static final int BIG_ATOMS_NUMBER = 50;
    private static final int NN_COUNT_DELTA = 5;

    private static final double NN_DISTANCE_DELTA_FOR_SMALLER_N = 0.01;
    private static final double NN_DISTANCE_DELTA_FOR_BIGGER_N = 0.0035;

    public int smallAtomsNumber;
    public int bigAtomsNumber;
    public int nnCountDelta;
    public double nnDistanceDeltaForSmallerN;
    public double nnDistanceDeltaForBiggerN;

    public ConformationFilterByNearestNeighbours() {
        this.smallAtomsNumber = SMALL_ATOMS_NUMBER;
        this.bigAtomsNumber = BIG_ATOMS_NUMBER;
        this.nnCountDelta = NN_COUNT_DELTA;
        this.nnDistanceDeltaForSmallerN = NN_DISTANCE_DELTA_FOR_SMALLER_N;
        this.nnDistanceDeltaForBiggerN = NN_DISTANCE_DELTA_FOR_BIGGER_N;
        setConfiguration();
    }

    public ConformationFilterByNearestNeighbours(int smallAtomsNumber, int bigAtomsNumber, int nnCountDelta,
            int nnDistanceDeltaForSmallerN, int nnDistanceDeltaForBiggerN) {
        this.smallAtomsNumber = smallAtomsNumber;
        this.bigAtomsNumber = bigAtomsNumber;
        this.nnCountDelta = nnCountDelta;
        this.nnDistanceDeltaForSmallerN = nnDistanceDeltaForSmallerN;
        this.nnDistanceDeltaForBiggerN = nnDistanceDeltaForBiggerN;
        setConfiguration();
    }

    @Override
    public Set<ClusterConformation> applyFilter(SortedSet<ClusterConformation> in) {
        Set<ClusterConformation> filteredSet = new TreeSet<>();
        // Check condition to validate filter
        if (!in.isEmpty()) {
            if (in.first().getAtomsNumber() > BIG_ATOMS_NUMBER)
                return filteredSet;
        }

        final ClusterConformation go = in.first();
        NearestNeighbourParameters goParams = NearestNeighbourAnalyzer.analyzeNearestNeighbourContacts(go);
        final int goNNCount = goParams.getNNCount();
        final double goNNDistance = goParams.getNNRelativeDistance();
        int maxNNCount = goNNCount;

        // Iterates for the first time to get maximum nn count
        for (ClusterConformation conformation : in) {
            NearestNeighbourParameters nnParameters = NearestNeighbourAnalyzer
                    .analyzeNearestNeighbourContacts(conformation);
            conformation.setNearestNeighbourParameters(nnParameters);
            if (nnParameters.getNNCount() > goNNCount) {
                maxNNCount = nnParameters.getNNCount();
            }
        }

        final int maxNN = maxNNCount;

        // Iterates for the second time to get rid of conformations with small nn count,
        // except those that make good relative nn distance
        //----------------------------------------------------------------------------------------------
        // NOTE: removeIf was originally supposed to be used in a more compact and elegant way, --------
        // the ugliness is due to the side-effect of adding removed conformation to filteredSet --------
        //-----------------------------------------------------------------------------------------------
        Iterables.removeIf(in, new Predicate<ClusterConformation>() {
            public boolean apply(ClusterConformation conformation) {
                NearestNeighbourParameters nnParameters = conformation.getNearestNeighbourParameters();
                if (nnParameters.getNNCount() < goNNCount) {
                    if (conformation.getAtomsNumber() < SMALL_ATOMS_NUMBER) {
                        if (nnParameters.getNNRelativeDistance() - goNNDistance > NN_DISTANCE_DELTA_FOR_SMALLER_N) {
                            filteredSet.add(conformation);
                            return true;
                        } else return false;
                    } else {
                        if (nnParameters.getNNRelativeDistance() - goNNDistance > NN_DISTANCE_DELTA_FOR_BIGGER_N) {
                            filteredSet.add(conformation);
                            return true;
                        } else return false;
                    }
                } else {
                    if ((maxNN - nnParameters.getNNCount()) > NN_COUNT_DELTA) {
                        filteredSet.add(conformation);
                        return true;
                    } else return false;
                }
            }
        });

        LOGGER.debug("Bulk filter {} filtered out {} conformations, max NNs: {}", this.getClass().getSimpleName(),
                filteredSet.size(), maxNNCount);

        return filteredSet;
    }
}
