package edu.ssau.acat.core.impl.generator;

import java.util.ArrayList;
import java.util.List;

import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ExtendedAtomicTripletGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtendedAtomicTripletGenerator.class);

    private ExtendedAtomicTripletGenerator() {
    }

    // TODO what's the meaning of the cross factor?
    public static final double XY_CROSS_FACTOR = 1.0e-15;

    public static final double RADIUS = 0.5;

    /**
     * Finds coordinates for "good" vacant positions, that might extend a triplet,
     * so that the corresponding potential energy value is minimal.
     * @param triplet group of three atoms to be extended
     * @return vacant atom positions
     */
    public static List<Vector3D> extendTriplet(AtomicTriplet triplet) {
        //LOGGER.debug("Extending triplet {}", triplet);
        List<Vector3D> candidatePositions;
        // Finds new triplet coordinates, since new atom position is to be calculated in the new coordinate system
        RealMatrix transformationMatrix = getTransformationMatrix(triplet);
        // Suppose origin is in the first atom position
        Vector3D origin = triplet.getFirst();
        AtomicTriplet tripletInNewCoordinates = getTripletInNewCoordinates(triplet, transformationMatrix, origin);
        Vector3D addedNewCoordinates = findAddedMemberNewCoordinates(tripletInNewCoordinates);
        // Changes back the coordinate system to obtain two possible positions for the new atom
        Vector3D abovePosition = findAddedMemberAboveSurfacePosition(addedNewCoordinates, transformationMatrix, origin);
        Vector3D belowPosition = findAddedMemberBelowSurfacePosition(addedNewCoordinates, transformationMatrix, origin);
        //LOGGER.debug("Extended triplet with two possible positions: {} and {}", abovePosition, belowPosition);
        candidatePositions = new ArrayList<Vector3D>();
        candidatePositions.add(abovePosition);
        candidatePositions.add(belowPosition);
        return candidatePositions;
    }

    public static RealMatrix getTransformationMatrix(AtomicTriplet triplet) {
        LOGGER.trace("Composing transformation matrix for triplet: {}", triplet);
        Vector3D n = getNormalVector(triplet);
        Vector3D e1 = getFirstUnitVector(triplet);
        Vector3D e2 = getSecondUnitVector(e1, n);
        // Composes the transformation matrix
        RealMatrix transformationMatrix = Vector3DHelper.getRealMatrixFromVector3Ds(e1, e2, n);
        LOGGER.trace("Composed {}x{} transformation matrix made of vectors n:{}, e1:{}, e2:{}",
                transformationMatrix.getRowDimension(), transformationMatrix.getColumnDimension(), n, e1, e2);
        return transformationMatrix;
    }

    public static AtomicTriplet getTripletInNewCoordinates(AtomicTriplet originalTriplet, RealMatrix transformationMatrix,
            Vector3D origin) {
        LOGGER.trace("Calculating triplet new coordinates, origin is {}", origin);
        AtomicTriplet newTriplet = new AtomicTriplet();
        // Finds new coordinates by solving equation
        RealMatrix inverseMatrix = new LUDecomposition(transformationMatrix).getSolver().getInverse();
        newTriplet.setFirst(getAtomNewCoordinates(inverseMatrix, originalTriplet.getFirst(), origin));
        newTriplet.setSecond(getAtomNewCoordinates(inverseMatrix, originalTriplet.getSecond(), origin));
        newTriplet.setThird(getAtomNewCoordinates(inverseMatrix, originalTriplet.getThird(), origin));
        LOGGER.trace("The triplet in new coordinates: {}", newTriplet);
        return newTriplet;
    }

    public static Vector3D findAddedMemberNewCoordinates(AtomicTriplet tripletInNewCoordinates) {
        LOGGER.trace("Finding position in the new coordinate system for the atom being added to triplet {}",
                tripletInNewCoordinates);
        Vector3D second = tripletInNewCoordinates.getSecond();
        Vector3D third = tripletInNewCoordinates.getThird();
        double x2 = second.getX();
        double y2 = second.getY();
        double x3 = third.getX();
        double y3 = third.getY();
        double crossFactor = x2 * y3 - y2 * x3;
        if (Math.abs(crossFactor) <= XY_CROSS_FACTOR) {
            LOGGER.warn("Cross factor value {} is too small", crossFactor);
        }
        // Calculates x y coordinates
        double x4 = solveEquationWRTX(x2, y2, x3, y3, crossFactor);
        double y4 = solveEquationWRTY(x2, y2, x3, y3, crossFactor);
        // Calculates z coordinate
        double z4 = getZCoordinate(x3, y3, x4, y4);
        Vector3D position = new Vector3D(x4, y4, z4);
        LOGGER.trace("Found position for the added atom (z should be taken with the opposite sign to get position on the "
                + "other side of the surface): {}", position);
        return position;
    }

    private static double getZCoordinate(double x3, double y3, double x4, double y4) {
        double r4 = getAddedAtomRadius(x3, y3, x4, y4);
        double rDifference = RADIUS * RADIUS - r4 * r4;
        if (rDifference < 0) {
            LOGGER.error("The radius difference can't be negative number.",
                    new UnsupportedOperationException("Can't take square root"));
        }
        return 2 * Math.sqrt(rDifference);
    }

    public static Vector3D findAddedMemberAboveSurfacePosition(Vector3D coordinates, RealMatrix transformationMatrix,
            Vector3D origin) {
        RealMatrix originMatrix = Vector3DHelper.getColumnRealMatrixFromVector3D(origin);
        return Vector3DHelper.getVector3DFromColumnMatrix(transformationMatrix.multiply(Vector3DHelper
                .getColumnRealMatrixFromVector3D(coordinates)).add(originMatrix));
    }

    public static Vector3D findAddedMemberBelowSurfacePosition(Vector3D coordinates, RealMatrix transformationMatrix,
            Vector3D origin) {
        // Flips z coordinate
        Vector3D belowCoordinates = new Vector3D(coordinates.getX(), coordinates.getY(), -coordinates.getZ());
        RealMatrix originMatrix = Vector3DHelper.getColumnRealMatrixFromVector3D(origin);
        return Vector3DHelper.getVector3DFromColumnMatrix(transformationMatrix.multiply(Vector3DHelper
                .getColumnRealMatrixFromVector3D(belowCoordinates)).add(originMatrix));
    }

    public static Vector3D getAtomNewCoordinates(RealMatrix inverseMatrix, Vector3D atomCoordinates, Vector3D origin) {
        return Vector3DHelper.getVector3DFromColumnMatrix(inverseMatrix.multiply(Vector3DHelper
                .getColumnRealMatrixFromVector3D(atomCoordinates.subtract(origin))));
    }

    /**
     * Returns the normal vector to the surface of the triplet.
     * @param triplet
     * @return
     */
    public static Vector3D getNormalVector(AtomicTriplet triplet) {
        // Extracts triplet elements
        Vector3D first = triplet.getFirst();
        Vector3D second = triplet.getSecond();
        Vector3D third = triplet.getThird();
        Vector3D x = new Vector3D(first.getX(), second.getX(), third.getX());
        Vector3D z = new Vector3D(first.getZ(), second.getZ(), third.getZ());
        Vector3D y = new Vector3D(first.getY(), second.getY(), third.getY());
        double nX = Vector3DHelper.reduceCrossProduct(y, z);
        double nY = -Vector3DHelper.reduceCrossProduct(x, z);
        double nZ = Vector3DHelper.reduceCrossProduct(x, y);
        return new Vector3D(nX, nY, nZ).normalize();
    }

    /**
     * Returns normalized first unit vector, having its head (ending ->) in the second atom position
     * and its tail in the first atom position.
     * @param triplet
     * @return
     */
    public static Vector3D getFirstUnitVector(AtomicTriplet triplet) {
        return triplet.getSecond().subtract(triplet.getFirst()).normalize();
    }

    /**
     * Returns second unit vector, having its head (ending ->) in the third atom position
     * and its tail in the first atom position.
     * @param firstUnitVector
     * @param normalVector
     * @return
     */
    public static Vector3D getSecondUnitVector(Vector3D firstUnitVector, Vector3D normalVector) {
        return firstUnitVector.crossProduct(normalVector);
    }

    private static double solveEquationWRTX(double a, double b, double c, double d, double crossFactor) {
        return -(b * (c * c + d * d) - d * (a * a + b * b) * 0.5 / crossFactor);
    }

    private static double solveEquationWRTY(double a, double b, double c, double d, double crossFactor) {
        return (a * (c * c + d * d) - c * (a * a + b * b)) * 0.5 / crossFactor;
    }

    private static double getAddedAtomRadius(double c, double d, double x4, double y4) {
        return Math.sqrt((x4 - c) * (x4 - c) + (y4 - d) * (y4 - d)) * 0.5;
    }
}
