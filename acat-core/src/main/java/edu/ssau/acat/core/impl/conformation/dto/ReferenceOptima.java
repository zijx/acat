package edu.ssau.acat.core.impl.conformation.dto;

import java.util.HashMap;
import java.util.Map;

import edu.ssau.acat.core.api.conformation.ClusterConformation;

public final class ReferenceOptima {

    private static Map<Integer, Double> optima;

    private ReferenceOptima() {
        optima = new HashMap<>();
        setOptima();
    }

    public boolean matchesGlobalOptimum(ClusterConformation conformation) {
        return (Math.abs(optima.get(conformation.getAtomsNumber()) - conformation.getPotentialValue()) < 0.0001);
    }

    private void setOptima() {
        optima.put(5, -9.04493);
        optima.put(6, -12.487810);
        optima.put(7, -16.207580);
        optima.put(8, -19.327420);
        optima.put(9, -23.417190);
        optima.put(10, -27.473283);
    }
}
