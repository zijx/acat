package edu.ssau.acat.core.impl.conformation.dto;

import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;

public final class MorseConformations {

    public static final double[] MORSE_CLUSTER_3 = {
            0.911228718694926, 0.000000000000000,  -0.000000000000119,
            0.333281226282783,  -0.814219731595469,  -0.000000000000040,
            0.044307480076643,   0.000000000000001,  -0.500517210482427
    };

    public static final double[] MORSE_CLUSTER_5A = {0.911228718694926, 0.000000000000000,  -0.000000000000119,
            0.333281226282784,   0.814219731595469,  -0.000000000000038,
            0.333281226282783,  -0.814219731595469,  -0.000000000000040,
            0.044307480076782,   0.000000000000000,   0.500517210482431,
            0.044307480076643,   0.000000000000001,  -0.500517210482427
    };

    public static final double[] MORSE_CLUSTER_6A = {0.497763270596722, 0.497763270596722, 0.000000000000000,
            -0.497763270596722, 0.497763270596722, 0.000000000000000,
            -0.497763270596722, -0.497763270596722, 0.000000000000000,
            0.497763270596722, -0.497763270596722, 0.000000000000000,
            0.000000000000000, 0.000000000000000, 0.703943568129073,
            0.000000000000000, 0.000000000000000, -0.703943568129073
    };

    public static final double[] MORSE_CLUSTER_7A = {0.9104573, -1.31E-11, -3.41E-10, 0.3208315, 0.8115503, 1.30E-09,
            0.3208315, -0.8115503, -6.19E-10, -0.6332031, 0.5015657, 2.04E-09, -0.6332031, -0.5015657, 8.53E-10,
            0.05714286, 3.28E-11, 0.5097267, 0.05714286, 2.02E-09, -0.5097267
    };

    public static final double[] MORSE_CLUSTER_7XB = {-4.982156e-001, -4.974136e-001, -5.563715e-004,
            4.982064e-001, -4.974193e-001, -5.102993e-004, -4.976781e-001, 4.969620e-001, 6.077077e-004,
            4.976787e-001, 4.969574e-001, 6.468595e-004, 1.086849e-005, 7.589977e-004, 7.040498e-001,
            -1.008545e-005, -7.510917e-004, -7.033345e-001, -4.241894e-006, -9.970918e-001, 7.051141e-001
    };

    public static final double[] MORSE_CLUSTER_19A = {
            0.025697930875637,  -0.019904657438067,   0.135672943218736,
            0.025749010572872,  -0.033360933962459,   1.089771851206393,
            0.288510512634308,  -0.852057068297245,   0.530114350584021,
            -0.679128501438903,  -0.530916282946204,   0.534695412235202,
            -0.672691295499432,   0.488509784899902,   0.549072695491413,
            0.298926130636954,   0.797408958495410,   0.553377283558456,
            0.892981518111460,  -0.031106920971917,   0.541660382035244,
            -0.246332889849556,  -0.832833744549500,  -0.336477370197010,
            -0.837778373102953,  -0.007957840641136,  -0.324811945339760,
            -0.235963031494855,   0.809385570174430,  -0.313316639859308,
            0.727424787823475,   0.489655674630855,  -0.317877575218576,
            0.721015862901747,  -0.525291678850094,  -0.332191693771547,
            0.025648611635506,  -0.006912150256111,  -0.785543032973217,
            0.025597531938271,   0.006544126268281,  -1.739641940960874,
            -0.679221304705514,  -0.506468479112360,  -1.198742727081013,
            0.288417709367696,  -0.827609264463402,  -1.203323788732194,
            -0.672784098766043,  0.512957588733745,  -1.184365443824802,
            0.298833327370342,  0.821856762329254,  -1.180060855757759,
            0.892888714844848, -0.006659117138074,  -1.191777757280971
    };

    public static final double[] MORSE_CLUSTER_28X = {
            0.8726835, 0.1052416, 0.2134061, 0.2071783, 0.8157555, 0.2303691, 0.362064, -0.758317, -0.0133527,
            -0.7118864, 0.4981106, 0.08554845, -0.6318161, -0.5041949, -0.061696, -0.00713704, -0.03378654, 0.6132884,
            0.05753303, 0.08326578, -0.3378284, 0.8845323, -0.2570406, -0.7289865, -0.2135628, 0.9833177, -0.6314304,
            -0.7078577, 0.1577761, -0.9003911, 0.2363451, 0.2749393, -1.271475, 0.6443203, 0.5823498, 1.052023,
            0.7254376, 0.6971044, -0.5419954, 0.4829208, 1.604445, -0.2689452, -0.06002815, -0.6449387, -0.9328666,
            -0.3780851, 0.8086391, 0.9857708, -0.452903, 1.495031, 0.2085457, 0.7433064, -0.600796, 0.8805713,
            0.4003753, 1.552154, 0.7840695, -0.9346141, -0.1311739, 0.8275799, 1.152851, 1.058992, 0.2605548,
            1.610612, 0.3626877, -0.3857123, 0.4964384, 1.260372, -1.274872, -0.4405508, 0.9084922, -1.575149,
            -0.2526606, -0.9500987, 0.7547982, 1.225358, 0.5187693, -1.327197, 1.363714, -0.6978889, 0.03032436,
            1.388527, 1.343738, -0.710405
    };

    public static final double POTENTIAL_FOR_7A = -16.207580;
    public static final double POTENTIAL_FOR_9B = -23.417190;
    public static final double POTENTIAL_FOR_6A = -12.487810;


    private MorseConformations() {
    }

    public static MorseClusterConformation createMorseConformation3() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_3), "3");
    }

    public static MorseClusterConformation createMorseConformation5A() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_5A), "5A");
    }

    public static MorseClusterConformation createMorseConformation6A() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_6A), "6A");
    }

    public static MorseClusterConformation createMorseConformation7A() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_7A), "7A");
    }

    public static MorseClusterConformation createMorseConformation7XB() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_7XB), "7*B");
    }

    public static MorseClusterConformation createMorseConformation19A() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_19A), "19A");
    }

    public static MorseClusterConformation createMorseConformation28X() {
        return new MorseClusterConformation(Vector3DHelper.getVector3DList(MORSE_CLUSTER_28X), "28*");
    }
}
