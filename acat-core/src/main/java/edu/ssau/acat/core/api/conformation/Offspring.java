package edu.ssau.acat.core.api.conformation;

public interface Offspring<T> {

    T getParent();

    void setParent(T parent);
}
