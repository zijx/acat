package edu.ssau.acat.core.samples.optimization;

import java.util.Arrays;

import edu.ssau.acat.core.impl.potential.morse.MorsePairPotentialFunction;
import edu.stanford.nlp.optimization.CGMinimizer;
import edu.stanford.nlp.optimization.Minimizer;
import edu.stanford.nlp.optimization.QNMinimizer;

public class Expanded28XConformationOptimizer {

    public static final int EXPANDED_DIMENSION = 29 * 3;

    public static final double[] EXPANDED_28A_COORDINATES = {
            0.8726835, 0.1052416, 0.2134061, 0.2071783, 0.8157555, 0.2303691, 0.362064, -0.758317, -0.0133527,
            -0.7118864, 0.4981106, 0.08554845, -0.6318161, -0.5041949, -0.061696, -0.00713704, -0.03378654, 0.6132884,
            0.05753303, 0.08326578, -0.3378284, 0.8845323, -0.2570406, -0.7289865, -0.2135628, 0.9833177, -0.6314304,
            -0.7078577, 0.1577761, -0.9003911, 0.2363451, 0.2749393, -1.271475, 0.6443203, 0.5823498, 1.052023,
            0.7254376, 0.6971044, -0.5419954, 0.4829208, 1.604445, -0.2689452, -0.06002815, -0.6449387, -0.9328666,
            -0.3780851, 0.8086391, 0.9857708, -0.452903, 1.495031, 0.2085457, 0.7433064, -0.600796, 0.8805713,
            0.4003753, 1.552154, 0.7840695, -0.9346141, -0.1311739, 0.8275799, 1.152851, 1.058992, 0.2605548,
            1.610612, 0.3626877, -0.3857123, 0.4964384, 1.260372, -1.274872, -0.4405508, 0.9084922, -1.575149,
            -0.2526606, -0.9500987, 0.7547982, 1.225358, 0.5187693, -1.327197, 1.363714, -0.6978889, 0.03032436,
            1.388527, 1.343738, -0.710405, -0.111758168, -0.000843673, 1.546722766
    };

    public static final int RO_PARAMETER = 6;

    public static final double TOLERANCE = 1e-12;

    public static final int MAX_ITERATIONS = 660;

    public static final MorsePairPotentialFunction POTENTIAL_FUNCTION = new MorsePairPotentialFunction(
            EXPANDED_DIMENSION, RO_PARAMETER);

    public void cgOptimizeAddedAtomCoordinates() {
        Minimizer cgMinimizer = new CGMinimizer();
        System.out.println("Initial potential value:" + POTENTIAL_FUNCTION.valueAt(EXPANDED_28A_COORDINATES));
        double [] solution = cgMinimizer.minimize(POTENTIAL_FUNCTION, TOLERANCE, EXPANDED_28A_COORDINATES, MAX_ITERATIONS);
        printSolution(solution);
    }

    public void qnOptimizeAddedAtomCoordinates() {
        // m - the number of previous estimated vector pairs to store
        int m = 5;
        Minimizer qnMinimizer = new QNMinimizer(m, true);
        System.out.println("Initial potential value:" + POTENTIAL_FUNCTION.valueAt(EXPANDED_28A_COORDINATES));
        double [] solution = qnMinimizer.minimize(POTENTIAL_FUNCTION, TOLERANCE, EXPANDED_28A_COORDINATES, MAX_ITERATIONS);
        printSolution(solution);
    }

    private void printSolution(double[] solution) {
        System.out.println("Solution: " + Arrays.toString(solution));
        System.out.println("Optimized potential value:" + POTENTIAL_FUNCTION.valueAt(solution));
    }

    public static void main(String[] args) {
        Expanded28XConformationOptimizer expanded7AConformationOptimizer = new Expanded28XConformationOptimizer();
        expanded7AConformationOptimizer.qnOptimizeAddedAtomCoordinates();
    }

}
