package edu.ssau.acat.core.api.filter.position;

import java.util.SortedSet;

import edu.ssau.acat.core.api.filter.Filter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;

/**
 * Filters out the whole bulk of candidate positions at a time.
 */
public interface BulkPositionFilter extends Filter {

    /**
     * Removes the elements in the set that have not passed the filter.
     * @param in
     * @return {@code true} if {@code in} set has been filtered (i.e. reduced), {@code false} otherwise
     */
    boolean applyFilter(SortedSet<WeightedPosition> in);

}
