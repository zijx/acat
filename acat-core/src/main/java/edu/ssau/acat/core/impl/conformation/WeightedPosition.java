package edu.ssau.acat.core.impl.conformation;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Position in 3-dimensional space with some weight associated with it.
 */
public class WeightedPosition implements Comparable<WeightedPosition> {

    private Vector3D position;

    private double weight;

    public WeightedPosition() {
    }

    public WeightedPosition(Vector3D position, double weight) {
        this.position = position;
        this.weight = weight;
    }

    public Vector3D getPosition() {
        return position;
    }

    public void setPosition(Vector3D position) {
        this.position = position;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public int compareTo(WeightedPosition o) {
        if (this.weight < o.getWeight()) {
            return -1;
        }
        if (this.weight > o.getWeight()) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "["
                + "position=" + position
                + ", weight=" + weight
                + "]";
    }
}
