package edu.ssau.acat.core.api.filter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFilter.class);

    protected Properties configuration = new Properties();

    protected void setConfiguration() {
        Class<? extends AbstractFilter> aClass = this.getClass();
        Field[] fields = aClass.getFields();
        for (Field field : fields) {
            if (!Modifier.isStatic(field.getModifiers())) {
                try {
                    configuration.setProperty(field.getName(), field.get(this).toString());
                } catch (IllegalAccessException e) {
                    LOGGER.error("Exception occurred while extracting filter fields by reflection", e);
                }
            }
        }
    }

    @Override
    public String getFilterName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public Properties getConfiguration() {
        return configuration;
    }
}
