package edu.ssau.acat.core.api.potential;

import java.util.List;

import edu.stanford.nlp.optimization.DiffFunction;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public interface PairPotentialFunction extends DiffFunction {

    double valueAt(List<Vector3D> argumentVector/*, double ... parameters*/);

    double[] derivativeAt(List<Vector3D> argumentVector);

    void setDimension(int dimension);

    /**
     * Returns contribution to potential energy value made by a pair of atoms.
     * @param distance distance between a given pair of atoms
     * @return contribution to potential by a pair of atoms
     */
    double pairValueAt(double distance/*, double ... parameters*/);

    double[] getParameters();

}
