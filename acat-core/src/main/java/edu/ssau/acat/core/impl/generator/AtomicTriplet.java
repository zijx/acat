package edu.ssau.acat.core.impl.generator;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class AtomicTriplet extends Triple<Vector3D, Vector3D, Vector3D> {

    private Vector3D first;
    private Vector3D second;
    private Vector3D third;

    public static AtomicTriplet of(Vector3D first, Vector3D second, Vector3D third) {
        return new AtomicTriplet(first, second, third);
    }

    public AtomicTriplet() {
    }

    public AtomicTriplet(Vector3D first, Vector3D second, Vector3D third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override public Vector3D getLeft() {
        return getFirst();
    }

    @Override public Vector3D getMiddle() {
        return getSecond();
    }

    @Override public Vector3D getRight() {
        return getThird();
    }

    public Vector3D getFirst() {
        return first;
    }

    public Vector3D getSecond() {
        return second;
    }

    public Vector3D getThird() {
        return third;
    }

    public void setFirst(Vector3D first) {
        this.first = first;
    }

    public void setSecond(Vector3D second) {
        this.second = second;
    }

    public void setThird(Vector3D third) {
        this.third = third;
    }
}
