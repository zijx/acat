package edu.ssau.acat.core.impl.conformation.nn;

// todo get nn pairs
public class NearestNeighbourParameters {

    private int nnCount;

    private double nnWeight;

    public NearestNeighbourParameters() {
    }

    public NearestNeighbourParameters(int nnCount, double nnWeight) {
        this.nnCount = nnCount;
        this.nnWeight = nnWeight;
    }

    public int getNNCount() {
        return nnCount;
    }

    public double getNNWeight() {
        return nnWeight;
    }

    public double getNNRelativeDistance() {
        return nnWeight / nnCount;
    }

    public void setNNCount(int nnCount) {
        this.nnCount = nnCount;
    }

    public void setNNWeight(double nnWeight) {
        this.nnWeight = nnWeight;
    }
}
