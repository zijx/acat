package edu.ssau.acat.core.impl.filter.position.iterative;

import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.position.IterativePositionFilter;
import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import edu.ssau.acat.core.impl.utilities.validation.ValidatingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class PositionFilterByContributionVector extends AbstractFilter implements IterativePositionFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionFilterByContributionVector.class);

    public static final double CONTRIBUTION_LOWER_LIMIT = 1.;
    public static final double RELATIVE_DIFFERENCE_TOLERANCE = 0.005;
    public static final double DIFFERENCE_TOLERANCE = 0.0001;

    @Override
    public boolean applyFilter(WeightedPosition in, SortedSet<WeightedPosition> out, ClusterConformation parent) {
        if (out.isEmpty()) {
            LOGGER.debug("No positions passed to filter against - candidate passes.");
        } else {
            PairPotentialFunction potentialFunction = parent.getPotentialFunction();
            int atomsNumber = parent.getAtomsNumber();
            double[] checkedContributions = parent.getContributionArray();
            ValidatingUtils.validateArrayLength(atomsNumber, checkedContributions);
            for (WeightedPosition position: out) {
                double[] contributions = parent.getContributionArray();
                ValidatingUtils.validateArrayLength(atomsNumber, contributions);
                for (int j = 0; j < contributions.length; j++) {
                    double jthContribution = checkedContributions[j];
                    if (Math.abs(jthContribution) > CONTRIBUTION_LOWER_LIMIT) {
                        if (exceedsRelativeDifference(jthContribution, contributions[j])) {
                            return false;
                        }
                    } else {
                        if (exceedsDifference(jthContribution, contributions[j])) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean exceedsRelativeDifference(double checked, double pivot) {
        return Math.abs((checked - pivot) / checked) > RELATIVE_DIFFERENCE_TOLERANCE;
    }

    private boolean exceedsDifference(double checked, double pivot) {
        return Math.abs(checked - pivot) > DIFFERENCE_TOLERANCE;
    }
}
