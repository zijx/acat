package edu.ssau.acat.core.impl.filter.pipeline;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.conformation.BulkConformationFilter;
import edu.ssau.acat.core.api.filter.conformation.IterativeConformationFilter;
import edu.ssau.acat.core.api.filter.position.BulkPositionFilter;
import edu.ssau.acat.core.api.filter.position.IterativePositionFilter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import edu.ssau.acat.core.impl.filter.conformation.bulk.Dregs;

public class FilterPipeline {

    private List<IterativePositionFilter> iterativePositionFilters;
    private List<BulkPositionFilter> bulkPositionFilters;

    private List<IterativeConformationFilter> iterativeConformationFilters;
    private List<BulkConformationFilter> bulkConformationFilters;

    private IterativePositionFilter lastIterativePositionFilter;
    private IterativeConformationFilter lastIterativeConformationFilter;

    public IterativePositionFilter getLastIterativePositionFilter() {
        return lastIterativePositionFilter;
    }

    public IterativeConformationFilter getLastIterativeConformationFilter() {
        return lastIterativeConformationFilter;
    }

    public FilterPipeline() {
        iterativePositionFilters = new ArrayList<>();
        bulkPositionFilters = new ArrayList<>();
        iterativeConformationFilters = new ArrayList<>();
        bulkConformationFilters = new ArrayList<>();
    }

    public void registerFilter(IterativePositionFilter newIterativePositionFilter) {
        iterativePositionFilters.add(newIterativePositionFilter);
    }

    public void registerFilter(BulkPositionFilter newBulkPositionFilter) {
        bulkPositionFilters.add(newBulkPositionFilter);
    }

    public void registerFilter(IterativeConformationFilter newIterativeConformationFilter) {
        iterativeConformationFilters.add(newIterativeConformationFilter);
    }

    public void registerFilter(BulkConformationFilter newBulkConformationFilter) {
        bulkConformationFilters.add(newBulkConformationFilter);
    }


    /**
     * Returns {@code true} if {@code in} has passed all iterativePositionFilters and been added to the {@code out} set,
     * returns {@code false} otherwise.
     * @param in
     * @param out
     * @param parent
     * @return
     */
    public boolean applyIterativePositionFilters(WeightedPosition in, SortedSet<WeightedPosition> out,
            ClusterConformation parent) {
        for (IterativePositionFilter iterativePositionFilter : iterativePositionFilters) {
            if (!iterativePositionFilter.applyFilter(in, out, parent)) {
                lastIterativePositionFilter = iterativePositionFilter;
                // If the candidate does not pass a filter, don't apply the rest of the filters
                return false;
            }
        }
        out.add(in);
        return true;
    }

    /**
     * Returns {@code true} if {@code in} has passed all filters and been added to the {@code out} set,
     * returns {@code false} otherwise.
     * @param in
     * @param out
     * @return
     */
    public boolean applyIterativeConformationFilters(ClusterConformation in, SortedSet<ClusterConformation> out) {
        for (IterativeConformationFilter iterativeConformationFilter : iterativeConformationFilters) {
            if (!iterativeConformationFilter.applyFilter(in, out)) {
                lastIterativeConformationFilter = iterativeConformationFilter;
                // If the candidate does not pass a filter, don't apply the rest of the filters
                return false;
            }
        }
        out.add(in);
        return true;
    }

    /**
     * Returns {@code true} if {@code in} has been filtered and some of its members have been removed.
     * @param in
     * @return
     */
    public boolean applyBulkPositionFilters(SortedSet<WeightedPosition> in) {
        boolean filtered = false;
        for (BulkPositionFilter bulkPositionFilter : bulkPositionFilters) {
            if (bulkPositionFilter.applyFilter(in)) {
                filtered = true;
            }
        }
        return filtered;
    }

    /**
     * Returns {@code true} if {@code in} has been filtered and some of its members have been removed.
     * @param in
     * @return
     */
    public List<Dregs> applyBulkConformationFilters(SortedSet<ClusterConformation> in) {
        List<Dregs> dregsList = new ArrayList<>();
        for (BulkConformationFilter bulkConformationFilter : bulkConformationFilters) {
            Set<ClusterConformation> removed = bulkConformationFilter.applyFilter(in);
            if (!removed.isEmpty()) {
                dregsList.add(new Dregs(removed, bulkConformationFilter));
            }
        }
        return dregsList;
    }

}
