package edu.ssau.acat.core.impl.problem;

public class ClusterConformationProblem {

    public static final int SPACE_DIMENSION = 3;

    /**
     * The parameter value to filter out atom candidates to make up 'triples' - group of three atoms that form a surface to
     * add another atom on.
     */
    public static final double MAX_PAIR_DISTANCE = 1.64;

}
