package edu.ssau.acat.core.api.potential;

public interface PairPotentialFunctionFactory {

    PairPotentialFunction createPairPotentialFunction(int dimension);

}
