package edu.ssau.acat.core.impl.filter.position.bulk;

import java.util.Iterator;
import java.util.SortedSet;

import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.position.BulkPositionFilter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PositionFilterByRelativePotentialVariance extends AbstractFilter implements BulkPositionFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionFilterByRelativePotentialVariance.class);

    public static final double RELATIVE_EMPIRICAL_VARIANCE_VALUE = 0.0005;
    public static final double FINE_CANDIDATES_PROPORTION = 0.25;

    public double relativePotentialVariance;
    public double fineCandidatesProportion;

    public PositionFilterByRelativePotentialVariance() {
        this.relativePotentialVariance = RELATIVE_EMPIRICAL_VARIANCE_VALUE;
        this.fineCandidatesProportion = FINE_CANDIDATES_PROPORTION;
        setConfiguration();
    }

    public PositionFilterByRelativePotentialVariance(double relativePotentialVariance, double fineCandidatesProportion) {
        this.relativePotentialVariance = relativePotentialVariance;
        this.fineCandidatesProportion = fineCandidatesProportion;
        setConfiguration();
    }

    @Override
    public boolean applyFilter(SortedSet<WeightedPosition> in) {
        int removedElementsCount = 0;
        int index = 1;
        // Filters only those candidates that follow the 'finest' part
        int afterFineIndex = (int) Math.ceil(in.size() * FINE_CANDIDATES_PROPORTION);
        Iterator<WeightedPosition> positionIterator = in.iterator();
        double recordPotential = positionIterator.next().getWeight();
        while (positionIterator.hasNext()) {
            if (index >= afterFineIndex) {
                WeightedPosition position = positionIterator.next();
                if (Math.abs(recordPotential - position.getWeight()) / recordPotential < RELATIVE_EMPIRICAL_VARIANCE_VALUE) {
                    positionIterator.remove();
                    //in.remove(position);
                    removedElementsCount++;
                }
            } else {
                index++;
                positionIterator.next();
            }
        }
        LOGGER.debug("Bulk filter {} filtered out {} candidates", this.getClass().getSimpleName(), removedElementsCount);
        return removedElementsCount > 0;
    }
}
