package edu.ssau.acat.core.impl.conformation;

public class WeightedDistanceMatrix {

    private double [][] distanceMatrix;

    // todo: probably, we don't need weights..
    private double [][] weightMatrix;

    private int dimension;

    public int getDimension() {
        return dimension;
    }

    public WeightedDistanceMatrix(int dimension) {
        this.dimension = dimension;
        distanceMatrix = new double[dimension][dimension];
        weightMatrix = new double[dimension][dimension];
    }

    public double getDistanceValueAt(int i, int j) {
        // todo check indices
        return distanceMatrix[i][j];
    }

    public void putDistanceValueAt(int i, int j, double distance) {
        // todo check indices
        // todo check distance for positivity
        distanceMatrix[i][j] = distance;
    }

    public double getWeightValueAt(int i, int j) {
        // todo check indices
        return weightMatrix[i][j];
    }

    public void putWeightValueAt(int i, int j, double distance) {
        // todo check indices
        // todo check distance for positivity
        weightMatrix[i][j] = distance;
    }
}
