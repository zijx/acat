package edu.ssau.acat.core.impl.potential.morse;

import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import edu.ssau.acat.core.api.potential.PairPotentialFunctionFactory;

public class MorsePairPotentialFunctionFactory implements PairPotentialFunctionFactory {

    @Override
    public PairPotentialFunction createPairPotentialFunction(int dimension) {
        return new MorsePairPotentialFunction(dimension);
    }
}
