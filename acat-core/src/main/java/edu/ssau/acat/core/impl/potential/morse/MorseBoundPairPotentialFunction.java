package edu.ssau.acat.core.impl.potential.morse;

import java.util.List;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.potential.BoundPairPotentialFunction;
import edu.ssau.acat.core.impl.conformation.AbstractClusterConformation;
import edu.ssau.acat.core.impl.problem.ClusterConformationProblem;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MorseBoundPairPotentialFunction extends BoundPairPotentialFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(MorseBoundPairPotentialFunction.class);

    public static final int DOMAIN_DIMENSION = ClusterConformationProblem.SPACE_DIMENSION;

    public MorseBoundPairPotentialFunction(AbstractClusterConformation conformation) {
        super(conformation);
    }

    @Override
    public int domainDimension() {
        return DOMAIN_DIMENSION;
    }

    @Override
    public double valueAt(double[] arguments, ClusterConformation conformation) {
        if (arguments.length != DOMAIN_DIMENSION) {
            throw new IllegalArgumentException("Three arguments expected, got " + arguments.length);
        }
        double potentialContribution = conformation.evaluateContribution(new Vector3D(arguments));
        return conformation.getPotentialValue() + potentialContribution;
    }

    @Override
    public double[] derivativeAt(double[] arguments, ClusterConformation conformation) {
        if (arguments.length != DOMAIN_DIMENSION) {
            throw new IllegalArgumentException("Three arguments expected, got " + arguments.length);
        }
        double rhoParameter;
        double[] parameters = conformation.getPotentialFunction().getParameters();
        if (parameters.length == 1) {
            rhoParameter = parameters[0];
        } else {
            throw new IllegalArgumentException("Expected 1 potential parameter, got " + parameters.length);
        }
        double distance;
        double derivativeOfPotential;
        Vector3D gradientElement;
        Vector3D derivativeOfDistance;
        Vector3D ithElement;
        Vector3D thisElement = new Vector3D(arguments);
        List<Vector3D> coordinates = conformation.getRelativeAtomicCoordinates();
        double[] gradientVector = new double[DOMAIN_DIMENSION];
        for (int i = 0; i < conformation.getAtomsNumber(); i++) {
            ithElement = coordinates.get(i);
            distance = Vector3D.distance(ithElement, thisElement);
            derivativeOfPotential = MorsePairPotentialFunction.derivativeOfPotentialWRTDistance(distance, rhoParameter);
            derivativeOfDistance = MorsePairPotentialFunction.derivativeOfDistanceWRTXYZ(distance, ithElement, thisElement);
            gradientElement = derivativeOfDistance.scalarMultiply(derivativeOfPotential);
            gradientVector[0] += gradientElement.getX();
            gradientVector[1] += gradientElement.getY();
            gradientVector[2] += gradientElement.getZ();
        }
        return gradientVector;
    }
}
