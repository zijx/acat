package edu.ssau.acat.core.api.filter;

import java.util.Properties;

public interface Filter {

    String getFilterName();

    Properties getConfiguration();
}
