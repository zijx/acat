package edu.ssau.acat.core.api.filter.position;

import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.Filter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;

/**
 * Filters out candidate positions iteratively - one new candidate at a time.
 */
public interface IterativePositionFilter extends Filter {

    /**
     * Returns {@code true} if {@code in} candidate passes the filter and should be added to {@code out},
     * returns {@code false} otherwise.
     * @param in
     * @param out
     * @param parent
     * @return {@code true} if {@code in} candidate passes the filter, {@code false} otherwise
     */
    boolean applyFilter(WeightedPosition in, SortedSet<WeightedPosition> out, ClusterConformation parent);

}
