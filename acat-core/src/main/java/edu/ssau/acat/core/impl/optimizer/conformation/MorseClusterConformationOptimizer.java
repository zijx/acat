package edu.ssau.acat.core.impl.optimizer.conformation;

/*public class MorseClusterConformationOptimizer implements ConformationOptimizer {

    public static final double TOLERANCE = 1e-12;
    public static final int MAX_ITERATIONS = 660;
    private static final int PREVIOUS_ESTIMATED_VECTORS_TO_STORE = 5;
    public static final boolean USE_ROBUST_OPTIONS = true;

    protected final Minimizer minimizer;

    protected double tolerance;

    protected int maxIterations;

    public MorseClusterConformationOptimizer(Minimizer minimizer, double tolerance, int maxIterations) {
        this.minimizer = minimizer;
        this.tolerance = tolerance;
        this.maxIterations = maxIterations;
    }

    public MorseClusterConformationOptimizer(Minimizer minimizer) {
        this.minimizer = minimizer;
        this.tolerance = TOLERANCE;
        this.maxIterations = MAX_ITERATIONS;
    }

    public MorseClusterConformationOptimizer() {
        minimizer = new QNMinimizer(PREVIOUS_ESTIMATED_VECTORS_TO_STORE, USE_ROBUST_OPTIONS);
        this.tolerance = TOLERANCE;
        this.maxIterations = MAX_ITERATIONS;
    }

    @Override
    public double optimize(ClusterConformation conformation) {
        double[] arguments = Vector3DHelper.getDoubleArrayFromVector3DList(conformation.getRelativeAtomicCoordinates());
        PairPotentialFunction potentialFunction = conformation.getPotentialFunction();
        List<Vector3D> optimizedPositions = Vector3DHelper.getVector3DList(minimizer.minimize(
                potentialFunction, tolerance, arguments, maxIterations));
        return potentialFunction.valueAt(optimizedPositions);
    }

}*/
