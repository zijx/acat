package edu.ssau.acat.core.api.conformation;

import java.util.List;

import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import edu.ssau.acat.core.impl.conformation.WeightedDistanceMatrix;
import edu.ssau.acat.core.impl.conformation.nn.NearestNeighbourParameters;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public interface ClusterConformation extends Offspring<ClusterConformation> {

    String getIdentifier();

    int getAtomsNumber();

    double getPotentialValue();

    Vector3D getRelativeAtomicCoordinate(int i);

    List<Vector3D> getRelativeAtomicCoordinates();

    WeightedDistanceMatrix getWeightedDistanceMatrix();

    PairPotentialFunction getPotentialFunction();

    //double getDeltaToGlobalOptimum();
    //BoundPairPotentialFunction getBoundPairPotentialFunction();

    /**
     * Returns array of energy contributions, so that i-th element corresponds to contribution of i-th atom.
     * Contribution of i-th atom is the total sum of pairwise potential values for this atom and each of
     * the atoms in the cluster.
     * @return
     */
    double[] getContributionArray();

    /**
     * Returns array of distances from {@code candidate} to each of atom's positions in the this conformation.
     * @param candidate
     * @return
     */
    double[] getDistanceArray(Vector3D candidate);

    void setIdentifier(String identifier);

    NearestNeighbourParameters getNearestNeighbourParameters();

    void setNearestNeighbourParameters(NearestNeighbourParameters nearestNeighbourParameters);

    double getGoDelta();

    void setGoDelta(double goDistance);

    /**
     * Evaluates contribution of any atom (a potential candidate to extend conformation) to potential energy value.
     * @param candidate
     * @return
     */
    double evaluateContribution(Vector3D candidate);
}
