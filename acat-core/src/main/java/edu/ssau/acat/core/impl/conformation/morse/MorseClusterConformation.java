package edu.ssau.acat.core.impl.conformation.morse;

import java.util.List;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.impl.conformation.AbstractClusterConformation;
import edu.ssau.acat.core.impl.potential.morse.MorsePairPotentialFunctionFactory;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MorseClusterConformation extends AbstractClusterConformation {

    private static final Logger LOGGER = LoggerFactory.getLogger(MorseClusterConformation.class);

    //private final BoundPairPotentialFunction boundPairPotentialFunction;

    private String identifier;

    private MorseClusterConformation parent;

    public MorseClusterConformation(List<Vector3D> atomicCoordinates) {
        super(atomicCoordinates, new MorsePairPotentialFunctionFactory());
        this.identifier = "";
        //this.boundPairPotentialFunction = new MorseBoundPairPotentialFunction(this);
    }

    public MorseClusterConformation(List<Vector3D> atomicCoordinates, MorseClusterConformation parent) {
        super(atomicCoordinates, new MorsePairPotentialFunctionFactory());
        this.parent = parent;
    }

    public MorseClusterConformation(List<Vector3D> atomicCoordinates, String identifier) {
        super(atomicCoordinates, new MorsePairPotentialFunctionFactory());
        this.identifier = identifier;
        //this.boundPairPotentialFunction = new MorseBoundPairPotentialFunction(this);
    }

/*    @Override
    public double getDeltaToGlobalOptimum() {
        return deltaToGlobalOptimum;
    }*/

    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    @Override
    public ClusterConformation getParent() {
        return this.parent;
    }

    @Override
    public void setParent(ClusterConformation parent) {
        if (!(parent instanceof MorseClusterConformation)) {
            throw new IllegalArgumentException("Expected parent of type " + MorseClusterConformation.class.getName());
        }
        this.parent = (MorseClusterConformation) parent;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }


/*    @Override
    public BoundPairPotentialFunction getBoundPairPotentialFunction() {
        return boundPairPotentialFunction;
    }*/

    @Override public String toString() {
        return new ToStringBuilder(this)
                .append("potential", getPotentialValue())
                .toString();
    }
}
