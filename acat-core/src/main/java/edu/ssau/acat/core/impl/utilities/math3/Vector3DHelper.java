package edu.ssau.acat.core.impl.utilities.math3;

import java.util.ArrayList;
import java.util.List;

import edu.ssau.acat.core.impl.problem.ClusterConformationProblem;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public final class Vector3DHelper {

    public static final int DIMENSION = ClusterConformationProblem.SPACE_DIMENSION;

    private Vector3DHelper() {
    }

    public static List<Vector3D> getVector3DList(double [] values) {
        // todo check if length can be divided by 3
        List<Vector3D> vectorList = new ArrayList<Vector3D>(values.length / DIMENSION);
        for (int i = 0; i < values.length; i += DIMENSION) {
            Vector3D vector3D = new Vector3D(values[i], values[i + 1], values[i + 2]);
            vectorList.add(vector3D);
        }
        return vectorList;
    }

    public static double[] getDoubleArrayFromVector3DList(List<Vector3D> vectors) {
        Vector3D vector3D;
        double[] values = new double[vectors.size() * DIMENSION];
        for (int i = 0, j = 0; j < vectors.size(); j++, i += DIMENSION) {
            vector3D = vectors.get(j);
            values[i] = vector3D.getX();
            values[i + 1] = vector3D.getY();
            values[i + 2] = vector3D.getZ();
        }
        return  values;
    }

    public static RealMatrix getColumnRealMatrixFromVector3D(Vector3D vector3D) {
        double [] matrixData = {vector3D.getX(), vector3D.getY(), vector3D.getZ()};
        return MatrixUtils.createColumnRealMatrix(matrixData);
    }

    // todo make valid for vararg
    public static RealMatrix getRealMatrixFromVector3Ds(Vector3D v1, Vector3D v2, Vector3D v3) {
        double[][] matrixData = {{v1.getX(), v2.getX(), v3.getX()}, {v1.getY(), v2.getY(), v3.getY()},
                {v1.getZ(), v2.getZ(), v3.getZ()}};
        return MatrixUtils.createRealMatrix(matrixData);
    }

    public static Vector3D getVector3DFromColumnMatrix(RealMatrix realMatrix) {
        //todo verify that matrix has 1 column
        //todo verify that matrix has 3 rows
        double[] matrixData = realMatrix.getColumn(0);
        return new Vector3D(matrixData[0], matrixData[1], matrixData[2]);
    }

    public static double reduceCrossProduct(Vector3D v1, Vector3D v2) {
        Vector3D product = v1.crossProduct(v2);
        return product.getX() + product.getY() + product.getZ();
    }

}
