package edu.ssau.acat.core.impl.optimizer;

import java.util.List;

import edu.ssau.acat.core.api.optimizer.PositionOptimizer;
import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;
import edu.stanford.nlp.optimization.QNMinimizer;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class BasicPositionOptimizer implements PositionOptimizer {

    public static final double TOLERANCE = 1e-12;
    public static final int MAX_ITERATIONS = 660;
    public static final int PREVIOUS_ESTIMATED_VECTORS_TO_STORE = 5;
    public static final boolean USE_ROBUST_OPTIONS = true;
    public static final boolean SUPPRESS_LOGS = true;

    // TODO could be Minimizer, but no common method found to suppress logging
    protected final QNMinimizer minimizer;

    protected double tolerance;

    protected int maxIterations;

    public BasicPositionOptimizer(QNMinimizer minimizer, boolean suppressLogs, double tolerance, int maxIterations) {
        this.minimizer = minimizer;
        this.tolerance = tolerance;
        this.maxIterations = maxIterations;
    }

    public BasicPositionOptimizer(QNMinimizer minimizer, double tolerance, int maxIterations) {
        this.minimizer = minimizer;
        this.tolerance = tolerance;
        this.maxIterations = maxIterations;
    }

    public BasicPositionOptimizer(QNMinimizer minimizer) {
        this.minimizer = minimizer;
        this.tolerance = TOLERANCE;
        this.maxIterations = MAX_ITERATIONS;
    }

    public BasicPositionOptimizer() {
        minimizer = new QNMinimizer(PREVIOUS_ESTIMATED_VECTORS_TO_STORE, USE_ROBUST_OPTIONS);
        this.tolerance = TOLERANCE;
        this.maxIterations = MAX_ITERATIONS;
    }

    public void setUpConfigurationOptions() {
        if (SUPPRESS_LOGS) {
            minimizer.shutUp();
        }
    }

    @Override
    public List<Vector3D> optimize(List<Vector3D> positions, PairPotentialFunction function) {
        double[] arguments = Vector3DHelper.getDoubleArrayFromVector3DList(positions);
        return Vector3DHelper.getVector3DList(minimizer.minimize(function, tolerance, arguments, maxIterations));
    }
}
