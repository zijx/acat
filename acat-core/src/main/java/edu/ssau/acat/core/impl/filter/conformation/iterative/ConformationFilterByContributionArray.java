package edu.ssau.acat.core.impl.filter.conformation.iterative;

import java.util.Arrays;
import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.conformation.IterativeConformationFilter;
import edu.ssau.acat.core.impl.utilities.validation.ValidatingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConformationFilterByContributionArray extends AbstractFilter implements IterativeConformationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConformationFilterByContributionArray.class);

    // These are magical empirical parameters discovered by prof. A.N. Kovartsev
    public static final double THRESHOLD_VALUE = 1.0;
    public static final double DIFFERENCE_TOLERANCE = 0.0001;
    public static final double RELATIVE_DIFFERENCE_TOLERANCE = 0.005;

    public double threshold;
    public double differenceTolerance;
    public double relativeDifferenceTolerance;

    public ConformationFilterByContributionArray() {
        this.threshold = THRESHOLD_VALUE;
        this.differenceTolerance = DIFFERENCE_TOLERANCE;
        this.relativeDifferenceTolerance = RELATIVE_DIFFERENCE_TOLERANCE;
        setConfiguration();
    }

    public ConformationFilterByContributionArray(double threshold, double differenceTolerance, double
            relativeDifferenceTolerance) {
        this.threshold = threshold;
        this.differenceTolerance = differenceTolerance;
        this.relativeDifferenceTolerance = relativeDifferenceTolerance;
        setConfiguration();
    }

    /**
     * Compares the incoming conformation against all the pivotal conformations {@code out},
     * that have already passed the filter.
     * @param inConformation
     * @param out
     * @return {@code false} if the {@code inConformation} should be considered 'unfit'
     * w.r.t. at least one conformation in {@code in}, returns {@code true} otherwise
     */
    @Override
    public boolean applyFilter(ClusterConformation inConformation, SortedSet<ClusterConformation> out) {
        if (out.isEmpty()) {
            LOGGER.debug("No conformations to filter against - candidate passed filter {}.", this.getClass().getSimpleName());
            return true;
        } else {
            boolean inPassesFilter;
            int atomsNumber = inConformation.getAtomsNumber();
            double[] inContributions = inConformation.getContributionArray();
            ValidatingUtils.validateArrayLength(atomsNumber, inContributions);
            Arrays.sort(inContributions);
            for (ClusterConformation outConformation : out) {
                inPassesFilter = false;
                if (outConformation.getAtomsNumber() != atomsNumber) {
                    throw new IllegalArgumentException("Conformations must have equal number of atoms");
                }
                double[] contributions = outConformation.getContributionArray();
                ValidatingUtils.validateArrayLength(atomsNumber, contributions);
                Arrays.sort(contributions);
                for (int j = 0; j < contributions.length && !inPassesFilter; j++) {
                    if (exceedsDifference(inContributions[j], contributions[j])) {
                        // If difference exceeds tolerance try next element in out
                        inPassesFilter = true;
                    }
                }
                if (!inPassesFilter) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean exceedsDifference(double testedContribution, double contribution) {
        if (Math.abs(testedContribution) > THRESHOLD_VALUE) {
            return Math.abs((testedContribution - contribution) / testedContribution) > RELATIVE_DIFFERENCE_TOLERANCE;
        } else {
            return Math.abs(testedContribution - contribution) > DIFFERENCE_TOLERANCE;
        }
    }
}
