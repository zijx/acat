package edu.ssau.acat.core.api.potential;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.impl.conformation.AbstractClusterConformation;
import edu.stanford.nlp.optimization.DiffFunction;

/**
 * Conformation-bound pair potential function evaluated in the single candidate atom position.
 */
public abstract class BoundPairPotentialFunction implements DiffFunction {

    private final AbstractClusterConformation conformation;

    public AbstractClusterConformation getConformation() {
        return conformation;
    }

    public BoundPairPotentialFunction(AbstractClusterConformation conformation) {
        this.conformation = conformation;
    }

    public abstract double valueAt(double[] arguments, ClusterConformation conformation);

    public double valueAt(double[] arguments) {
        return valueAt(arguments, conformation);
    }

    public abstract double[] derivativeAt(double[] arguments, ClusterConformation conformation);

    public double[] derivativeAt(double[] arguments) {
        return derivativeAt(arguments, conformation);
    }
}
