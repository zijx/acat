package edu.ssau.acat.core.impl.filter.position.iterative;

import java.util.Arrays;
import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.position.IterativePositionFilter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import edu.ssau.acat.core.impl.utilities.validation.ValidatingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PositionFilterByDistanceVector extends AbstractFilter implements IterativePositionFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionFilterByDistanceVector.class);

    public static final double DIFFERENCE_TOLERANCE = 0.000001;

    public double differenceTolerance;

    public PositionFilterByDistanceVector() {
        this.differenceTolerance = DIFFERENCE_TOLERANCE;
        setConfiguration();
    }

    public PositionFilterByDistanceVector(double differenceTolerance) {
        this.differenceTolerance = differenceTolerance;
        setConfiguration();
    }

    /**
     * Filters out isomorphic atom positions - two atom positions are considered 'isomorphic' iff
     * their distance vectors are identical with tolerance {@code DIFFERENCE_TOLERANCE}.
     * @param in
     * @param out
     * @param parent
     * @return
     */
    @Override
    public boolean applyFilter(WeightedPosition in, SortedSet<WeightedPosition> out, ClusterConformation parent) {
        if (out.isEmpty()) {
            // Any non-empty position is not isomorphic to *empty* position
            LOGGER.debug("No positions to filter against - candidate passed filter {}.", this.getClass().getSimpleName());
            return true;
        } else {
            int atomsNumber = parent.getAtomsNumber();
            double[] inDistances = parent.getDistanceArray(in.getPosition());
            ValidatingUtils.validateArrayLength(atomsNumber, inDistances);
            Arrays.sort(inDistances);
            for (WeightedPosition weightedPosition : out) {
                double[] distances = parent.getDistanceArray(weightedPosition.getPosition());
                ValidatingUtils.validateArrayLength(atomsNumber, distances);
                Arrays.sort(distances);
                for (int j = 0; j < distances.length; j++) {
                    if (exceedsDifference(inDistances[j], distances[j])) {
                        // If difference exceeds tolerance position should pass this filter
                        //LOGGER.debug("Candidate passed iterative filter {}", this.getClass().getSimpleName());
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean exceedsDifference(double distance1, double distance2) {
        return Math.abs(distance1 - distance2) > DIFFERENCE_TOLERANCE;
    }
}
