package edu.ssau.acat.core.impl.generator;

import java.util.ArrayList;
import java.util.List;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.impl.conformation.WeightedDistanceMatrix;
import edu.ssau.acat.core.impl.problem.ClusterConformationProblem;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class AtomicTripletsExtractor {

    private double maxDistance;

    public AtomicTripletsExtractor(double maxDistance) {
        this.maxDistance = maxDistance;
    }

    public AtomicTripletsExtractor() {
        this.maxDistance = ClusterConformationProblem.MAX_PAIR_DISTANCE;
    }

    public double getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(double maxDistance) {
        this.maxDistance = maxDistance;
    }

    public List<AtomicTriplet> extractTriplets(ClusterConformation conformation) {
        WeightedDistanceMatrix weightedDistanceMatrix = conformation.getWeightedDistanceMatrix();
        List<Vector3D> coordinates = conformation.getRelativeAtomicCoordinates();
        List<AtomicTriplet> triplets = new ArrayList<AtomicTriplet>();
        int atomsNumber = weightedDistanceMatrix.getDimension();
        // Iterates over distance matrix in O(n^3)
        for (int i = 0; i < atomsNumber; i++) {
            for (int j = i + 1; j < atomsNumber; j++) {
                if (areCloseEnough(weightedDistanceMatrix, i, j)) {
                    for (int k = j + 1; k < atomsNumber; k++) {
                        if (areCloseEnough(weightedDistanceMatrix, i, k)) {
                            if (areCloseEnough(weightedDistanceMatrix, j, k)) {
                                triplets.add(AtomicTriplet.of(coordinates.get(i), coordinates.get(j), coordinates.get(k)));
                            }
                        }
                    }
                }
            }
        }
        return triplets;
    }

    private boolean areCloseEnough(WeightedDistanceMatrix weightedDistanceMatrix, int j, int k) {
        return weightedDistanceMatrix.getDistanceValueAt(j, k) < maxDistance;
    }

}
