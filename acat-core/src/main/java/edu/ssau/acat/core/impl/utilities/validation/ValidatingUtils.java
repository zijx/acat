package edu.ssau.acat.core.impl.utilities.validation;

import static com.google.common.base.Preconditions.checkArgument;

public final class ValidatingUtils {

    private ValidatingUtils() {
    }

    public static void validateArrayLength(int length, double[] array) {
        checkArgument(array.length == length, "The length of array should equal" + length);
    }

}
