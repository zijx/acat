package edu.ssau.acat.core.api.optimizer;

import java.util.List;

import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public interface PositionOptimizer {

    List<Vector3D> optimize(List<Vector3D> positions, PairPotentialFunction function);

}
