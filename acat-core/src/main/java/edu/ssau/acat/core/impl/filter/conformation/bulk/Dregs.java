package edu.ssau.acat.core.impl.filter.conformation.bulk;

import java.util.Collection;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.conformation.BulkConformationFilter;

/**
 * Represents information about what has been filtered out by {@code BulkConformationFilter}.
 */
public class Dregs {

    private Collection<ClusterConformation> filteredConformations;

    private BulkConformationFilter filter;

    public Dregs() {
    }

    public Dregs(Collection<ClusterConformation> filteredConformations, BulkConformationFilter filter) {
        this.filteredConformations = filteredConformations;
        this.filter = filter;
    }

    public Collection<ClusterConformation> getFilteredConformations() {
        return filteredConformations;
    }

    public void setFilteredConformations(Collection<ClusterConformation> filteredConformations) {
        this.filteredConformations = filteredConformations;
    }

    public BulkConformationFilter getFilter() {
        return filter;
    }

    public void setFilter(BulkConformationFilter filter) {
        this.filter = filter;
    }
}
