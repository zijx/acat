package edu.ssau.acat.core.impl.filter.conformation.bulk;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.conformation.BulkConformationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConformationFilterByPotentialVariance extends AbstractFilter implements BulkConformationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConformationFilterByPotentialVariance.class);

    public static final double DEFAULT_VARIANCE_VALUE =  6.;
    public static final double MIN_VARIANCE_VALUE = 3.1;

    public static final int BIG_CANDIDATES_NUMBER = 3000;
    public static final int SMALL_CANDIDATES_NUMBER = 150;

    public double potentialVariance;
    public double defaultVarianceValue;
    public double minVarianceValue;
    public double bigCandidatesNumber;
    public double smallCandidatesNumber;

    public ConformationFilterByPotentialVariance() {
        this.potentialVariance = DEFAULT_VARIANCE_VALUE;
        this.defaultVarianceValue = DEFAULT_VARIANCE_VALUE;
        this.minVarianceValue = MIN_VARIANCE_VALUE;
        this.bigCandidatesNumber = BIG_CANDIDATES_NUMBER;
        this.smallCandidatesNumber = SMALL_CANDIDATES_NUMBER;
        setConfiguration();
    }

    @Override
    public Set<ClusterConformation> applyFilter(SortedSet<ClusterConformation> in) {
        Set<ClusterConformation> filteredSet = new TreeSet<>();

        // Sets potential variance value
        if (!in.isEmpty()) {
            if (in.size() <= BIG_CANDIDATES_NUMBER && in.size() >= SMALL_CANDIDATES_NUMBER) {
                potentialVariance = DEFAULT_VARIANCE_VALUE;
            } else if (in.size() > BIG_CANDIDATES_NUMBER) {
                potentialVariance = MIN_VARIANCE_VALUE;
            } else {
                return filteredSet;
            }
        }

        Iterator<ClusterConformation> conformationIterator = in.iterator();
        // Since the set is sorted the first element corresponds to minimal potential value
        double recordPotential = conformationIterator.next().getPotentialValue();
        while (conformationIterator.hasNext()) {
            ClusterConformation conformation = conformationIterator.next();
            if (Math.abs(recordPotential - conformation.getPotentialValue()) > potentialVariance) {
                filteredSet.add(conformation);
                conformationIterator.remove();
                //in.remove(position);
            }
        }
        LOGGER.debug("Bulk filter {} filtered out {} conformations", this.getClass().getSimpleName(), filteredSet.size());
        return filteredSet;
    }
}
