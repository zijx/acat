package edu.ssau.acat.core.impl.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.utilities.math3.Vector3DHelper;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MorseConformationSamplePersistenceService {

    public static final int DEFAULT_PORT = 27017;

    public static final String CONFORMATION_TABLE_NAME = "conformation";
    public static final String LINEAGE_TABLE_NAME = "lineage";

    private static final Logger LOGGER = LoggerFactory.getLogger(MorseConformationSamplePersistenceService.class);

    private DB dbInstance;

    private MongoClient mongoClient;

    private DBCollection conformationCollection;
    private DBCollection lineageCollection;

    /**
     *  Creates basic Mongo client.
     */
    public void createUnauthenticatedConnection() {
        mongoClient = new MongoClient("localhost", DEFAULT_PORT);
        dbInstance = mongoClient.getDB("clusterDB");
        prepareCollections(dbInstance);
        //LOGGER.debug("Collections prepared.");
    }

    private void prepareCollections(DB database) {
        try {
            conformationCollection = database.getCollection(CONFORMATION_TABLE_NAME);
            //lineageCollection = database.getCollection(LINEAGE_TABLE_NAME);
        } catch (MongoException e) {
            LOGGER.error("Couldn't create document", e);
        }
    }

    public void closeUnauthenticatedConnection() {
        mongoClient.close();
    }

    public void insertConformation(ClusterConformation conformation) {
        double[] coordinates = Vector3DHelper.getDoubleArrayFromVector3DList(conformation
                .getRelativeAtomicCoordinates());
        BasicDBObject doc = new BasicDBObject("name", conformation.getIdentifier())
                .append("potential", conformation.getPotentialValue())
                .append("n", conformation.getAtomsNumber())
                .append("coordinates", coordinates);
        conformationCollection.insert(doc);
    }

    public void bulkInsertConformations(Set<ClusterConformation> conformations) {
        LOGGER.debug("Inserting {} conformations..", conformations.size());
        BulkWriteOperation bulkWriteOperation = conformationCollection.initializeUnorderedBulkOperation();

        for (ClusterConformation conformation : conformations) {
            bulkWriteOperation.insert(new BasicDBObject("n", conformation.getAtomsNumber())
                    //.append("delta", conformation.getAtomsNumber())
                    .append("potential", conformation.getPotentialValue())
                    .append("coordinates", Vector3DHelper.getDoubleArrayFromVector3DList(conformation
                            .getRelativeAtomicCoordinates())));
        }

        BulkWriteResult result = bulkWriteOperation.execute();
        LOGGER.debug("Bulk inserted {} conformations", result.getInsertedCount());
    }

    public void debugFindConformationsByAtomsNumber(int atomsNumber) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("n", atomsNumber);
        DBCursor dbObjects = conformationCollection.find(searchQuery);
        while (dbObjects.hasNext()) {
            System.out.println(dbObjects.next());
        }
    }

    public int findNumber(int atomsNumber) {
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("n", atomsNumber);
        return conformationCollection.find(searchQuery).size();
    }

    public Set<ClusterConformation> findConformationsByAtomsNumber(int atomsNumber) {
        LOGGER.debug("Finding conformations with {} atoms..", atomsNumber);
        SortedSet<ClusterConformation> foundConformations = new TreeSet<ClusterConformation>();

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("n", atomsNumber);
        DBCursor dbObjects = conformationCollection.find(searchQuery);
        while (dbObjects.hasNext()) {
            // This is the way mongo handles arrays..
            BasicDBList basicDBList = (BasicDBList) dbObjects.next().get("coordinates");
            List<Vector3D> vectorList = new ArrayList<Vector3D>(basicDBList.size() / 3);
            for (int i = 0; i < basicDBList.size(); i += 3) {
                Vector3D vector3D = new Vector3D(
                        (Double) basicDBList.get(i), (Double) basicDBList.get(i + 1), (Double) basicDBList.get(i + 2));
                vectorList.add(vector3D);
            }
            foundConformations.add(new MorseClusterConformation(vectorList));
        }
        LOGGER.debug("Found {} conformations with {} atoms", foundConformations.size());
        return foundConformations;
    }

    public static void main(String[] args) {
        MorseConformationSamplePersistenceService persistenceService = new MorseConformationSamplePersistenceService();
        persistenceService.createUnauthenticatedConnection();
        persistenceService.closeUnauthenticatedConnection();
    }
}
