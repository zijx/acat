package edu.ssau.acat.core.impl.conformation.nn;

import edu.ssau.acat.core.api.conformation.ClusterConformation;

public final class NearestNeighbourAnalyzer {

    /**
     * These values are obtained from W. Pullan 2010
     */
    public static final double NEAREST_NEIGHBOUR_CRITERION = 1.123;
    public static final double NN_LOWER_THRESHOLD = 0.81;
    public static final double NN_UPPER_THRESHOLD = 1.54;

    public static final double NEAREST_NEIGHBOUR_TOLERANCE = 0.1;

    private NearestNeighbourAnalyzer() {
    }

    public static NearestNeighbourParameters analyzeNearestNeighbourContacts(ClusterConformation conformation) {
        int nnCount = 0;
        double nnWeight = 0.;
        int atomsNumber = conformation.getAtomsNumber();
        double distanceDelta;
        double squareDistanceDelta;
        for (int i = 0; i < atomsNumber; i++) {
            for (int j = i + 1; j < atomsNumber; j++) {
                distanceDelta = Math.abs(conformation.getWeightedDistanceMatrix().getDistanceValueAt(i, j)
                        - NEAREST_NEIGHBOUR_CRITERION);
                squareDistanceDelta = distanceDelta * distanceDelta;
                if (squareDistanceDelta < NEAREST_NEIGHBOUR_TOLERANCE) {
                    nnCount++;
                    nnWeight += distanceDelta;
                }
            }
        }
        return new NearestNeighbourParameters(nnCount, nnWeight);
    }
}
