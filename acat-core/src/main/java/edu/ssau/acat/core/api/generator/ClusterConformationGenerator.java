package edu.ssau.acat.core.api.generator;

import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;

public interface ClusterConformationGenerator<C extends ClusterConformation> {

    SortedSet<C> generateChildConformations(C parentConformation);

}
