package edu.ssau.acat.core.impl.conformation;

import java.util.Collections;
import java.util.List;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.potential.PairPotentialFunction;
import edu.ssau.acat.core.api.potential.PairPotentialFunctionFactory;
import edu.ssau.acat.core.impl.conformation.nn.NearestNeighbourParameters;
import edu.ssau.acat.core.impl.problem.ClusterConformationProblem;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public abstract class AbstractClusterConformation implements ClusterConformation, Comparable<AbstractClusterConformation> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractClusterConformation.class);

    public static final int SPACE_DIMENSION = ClusterConformationProblem.SPACE_DIMENSION;

    private final List<Vector3D> relativeAtomicCoordinates;

    private final WeightedDistanceMatrix weightedDistanceMatrix;

    /**
     * Number of atoms in the conformation.
     */
    private final int atomsNumber;

    private final double potentialValue;

    private double goDelta;

    private PairPotentialFunctionFactory pairPotentialFunctionFactory;

    private PairPotentialFunction potentialFunction;

    private NearestNeighbourParameters nearestNeighbourParameters;

    public AbstractClusterConformation(List<Vector3D> atomicCoordinates,
            PairPotentialFunctionFactory potentialFunctionFactory) {
        if (atomicCoordinates.isEmpty()) {
            throw new IllegalArgumentException("List of atomicCoordinates should not be empty.");
        }
        this.relativeAtomicCoordinates = atomicCoordinates;
        this.atomsNumber = atomicCoordinates.size();
        this.weightedDistanceMatrix = new WeightedDistanceMatrix(atomsNumber);
        this.pairPotentialFunctionFactory = potentialFunctionFactory;
        this.potentialFunction = pairPotentialFunctionFactory.createPairPotentialFunction(atomsNumber * SPACE_DIMENSION);
        double potentialSum = 0;
        for (int i = 0; i < atomsNumber; i++) {
            // todo do not set weighted matrix in constructor
            potentialSum += setWeightedDistanceMatrixColumn(i);
        }
        this.potentialValue = potentialSum;
    }

    @Override
    public Vector3D getRelativeAtomicCoordinate(int i) {
        checkArgument(i < relativeAtomicCoordinates.size());
        return relativeAtomicCoordinates.get(i);
    }

    @Override
    public int getAtomsNumber() {
        return atomsNumber;
    }

    @Override
    public PairPotentialFunction getPotentialFunction() {
        return potentialFunction;
    }

    @Override
    public List<Vector3D> getRelativeAtomicCoordinates() {
        return Collections.unmodifiableList(relativeAtomicCoordinates);
    }

    @Override
    public WeightedDistanceMatrix getWeightedDistanceMatrix() {
        return weightedDistanceMatrix;
    }

    @Override
    public double getPotentialValue() {
        return potentialValue;
    }

    @Override
    public NearestNeighbourParameters getNearestNeighbourParameters() {
        return nearestNeighbourParameters;
    }

    @Override
    public void setNearestNeighbourParameters(NearestNeighbourParameters nearestNeighbourParameters) {
        this.nearestNeighbourParameters = nearestNeighbourParameters;
    }

    @Override
    public double getGoDelta() {
        return goDelta;
    }

    @Override
    public void setGoDelta(double goDelta) {
        this.goDelta = goDelta;
    }

    @Override
    public double evaluateContribution(Vector3D candidate) {
        checkNotNull(candidate, "candidate can't be null");
        Vector3D ithElement;
        double pairDistance, pairPotential;
        double candidateContribution = 0;
        for (int row = 0; row < atomsNumber; row++) {
            ithElement = getRelativeAtomicCoordinate(row);
            pairDistance = Vector3D.distance(ithElement, candidate);
            pairPotential = potentialFunction.pairValueAt(pairDistance);
            candidateContribution += pairPotential;
        }
        return candidateContribution;
    }

    @Override
    public double[] getContributionArray() {
        double[] contributions = new double[atomsNumber];
        for (int i = 0; i < atomsNumber; i++) {
            for (int j = i + 1, k = i - 1; j < atomsNumber || k >= 0; j++, k--) {
                if (j < atomsNumber) {
                    contributions[i] += weightedDistanceMatrix.getWeightValueAt(i, j);
                }
                if (k >= 0) {
                    contributions[i] += weightedDistanceMatrix.getWeightValueAt(k, i);
                }
            }
        }
        return contributions;
    }

    @Override
    public double[] getDistanceArray(Vector3D candidate) {
        checkNotNull(candidate, "candidate can't be null");
        double[] distances = new double[atomsNumber];
        Vector3D ithElement;
        for (int row = 0; row < atomsNumber; row++) {
            ithElement = getRelativeAtomicCoordinate(row);
            distances[row] = Vector3D.distance(ithElement, candidate);
        }
        return distances;
    }

    /**
     * Sets upper diagonal weighted distance matrix for {@code column}-th atom if {@code column}<{@code atomsNumber}.
     * Throws IllegalArgumentException otherwise.
     * @param column
     * @return contribution of {@code column}-th atom to potential energy value
     */
    protected double setWeightedDistanceMatrixColumn(int column) {
        checkNotNull(potentialFunction);
        checkArgument(column < atomsNumber, "Column should be less than the number of atoms in the conformation.");
        Vector3D jthElement = getRelativeAtomicCoordinate(column);
        Vector3D ithElement;
        double pairDistance, pairPotential;
        double potentialSum = 0;
        for (int row = column - 1; row >= 0; row--) {
            ithElement = getRelativeAtomicCoordinate(row);
            pairDistance = Vector3D.distance(ithElement, jthElement);
            pairPotential = potentialFunction.pairValueAt(pairDistance);
            weightedDistanceMatrix.putDistanceValueAt(row, column, pairDistance);
            weightedDistanceMatrix.putWeightValueAt(row, column, pairPotential);
            potentialSum += pairPotential;
        }
        return potentialSum;

    }

    @Override
    public int compareTo(AbstractClusterConformation o) {
        if (this.potentialValue < o.getPotentialValue()) {
            return -1;
        }
        if (this.potentialValue > o.getPotentialValue()) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractClusterConformation that = (AbstractClusterConformation) o;

        if (Double.compare(that.potentialValue, potentialValue) != 0) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(potentialValue);
        return (int) (temp ^ (temp >>> 32));
    }
}
