package edu.ssau.acat.core.api.optimizer.conformation;

import edu.ssau.acat.core.api.conformation.ClusterConformation;

public interface ConformationOptimizer {

    double optimize(ClusterConformation conformation);

}
