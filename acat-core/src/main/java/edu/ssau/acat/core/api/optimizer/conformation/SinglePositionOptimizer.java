package edu.ssau.acat.core.api.optimizer.conformation;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public interface SinglePositionOptimizer {

    Vector3D optimizePositionForConformation(Vector3D position, ClusterConformation conformation);

}
