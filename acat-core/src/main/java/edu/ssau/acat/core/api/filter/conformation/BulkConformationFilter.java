package edu.ssau.acat.core.api.filter.conformation;

import java.util.Set;
import java.util.SortedSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.filter.Filter;

/**
 * Filters out the whole bulk of candidate conformations at a time.
 */
public interface BulkConformationFilter extends Filter {

    /**
     * Removes the elements in the set that have not passed the filter.
     * @param in
     * @return conformations that have been removed by the filter
     */
    Set<ClusterConformation> applyFilter(SortedSet<ClusterConformation> in);
}
