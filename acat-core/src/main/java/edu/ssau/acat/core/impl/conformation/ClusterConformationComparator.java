package edu.ssau.acat.core.impl.conformation;

import java.io.Serializable;
import java.util.Comparator;

import edu.ssau.acat.core.api.conformation.ClusterConformation;

public class ClusterConformationComparator implements Comparator<ClusterConformation>, Serializable {

    @Override
    public int compare(ClusterConformation a, ClusterConformation b) {
        if (a.getPotentialValue() < b.getPotentialValue()) {
            return -1;
        }
        if (a.getPotentialValue() > b.getPotentialValue()) {
            return 1;
        }
        return 0;
    }
}
