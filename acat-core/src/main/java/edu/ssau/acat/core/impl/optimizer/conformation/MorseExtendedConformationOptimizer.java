package edu.ssau.acat.core.impl.optimizer.conformation;
/*

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.optimizer.conformation.SinglePositionOptimizer;
import edu.ssau.acat.core.impl.optimizer.conformation.MorseClusterConformationOptimizer;
import edu.stanford.nlp.optimization.CGMinimizer;
import edu.stanford.nlp.optimization.Minimizer;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class MorseExtendedConformationOptimizer extends MorseClusterConformationOptimizer implements SinglePositionOptimizer {

    public static final boolean SILENT = false;
    private final Minimizer singlePositionMinimizer;

    public MorseExtendedConformationOptimizer(Minimizer minimizer, Minimizer singlePositionMinimizer,
            double tolerance, int maxIterations) {
        super(minimizer, tolerance, maxIterations);
        this.singlePositionMinimizer = singlePositionMinimizer;
    }

    public MorseExtendedConformationOptimizer(Minimizer minimizer, Minimizer singlePositionMinimizer) {
        super(minimizer);
        this.singlePositionMinimizer = singlePositionMinimizer;
    }

    public MorseExtendedConformationOptimizer() {
        this.singlePositionMinimizer = new CGMinimizer(SILENT);
    }

    @Override
    public Vector3D optimizePositionForConformation(Vector3D position, ClusterConformation conformation) {
        return new Vector3D(singlePositionMinimizer.minimize(conformation.getBoundPairPotentialFunction(), tolerance,
                position.toArray(), maxIterations));
    }
}
*/
