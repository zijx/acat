package edu.ssau.acat.core.impl.search;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.ssau.acat.core.api.conformation.ClusterConformation;
import edu.ssau.acat.core.api.generator.ClusterConformationGenerator;
import edu.ssau.acat.core.impl.conformation.dto.MorseConformations;
import edu.ssau.acat.core.impl.conformation.morse.MorseClusterConformation;
import edu.ssau.acat.core.impl.conformation.nn.NearestNeighbourAnalyzer;
import edu.ssau.acat.core.impl.conformation.nn.NearestNeighbourParameters;
import edu.ssau.acat.core.impl.filter.conformation.iterative.ConformationFilterByContributionArray;
import edu.ssau.acat.core.impl.filter.pipeline.FilterPipeline;
import edu.ssau.acat.core.impl.filter.position.iterative.PositionFilterByDistanceVector;
import edu.ssau.acat.core.impl.generator.MorseClusterConformationGenerator;
import edu.ssau.acat.core.impl.persistence.MorseConformationSamplePersistenceService;
import edu.ssau.acat.core.impl.utilities.logging.LoggingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Presents a really 'quick and dirty' solution to play around with sample problems.
 */
public class ClusterSearchTreeBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterSearchTreeBuilder.class);
    public static final int STARTING_DIMENSION = 18;
    public static final int TARGET_DIMENSION = 19;

    private final FilterPipeline filterPipeline;

    private final MorseConformationSamplePersistenceService persistenceService =
            new MorseConformationSamplePersistenceService();

    private final ClusterConformationGenerator conformationGenerator;

    public ClusterSearchTreeBuilder(FilterPipeline filterPipeline, ClusterConformationGenerator conformationGenerator) {
        this.filterPipeline = filterPipeline;
        this.conformationGenerator = conformationGenerator;
    }

    public void startSearch(int startingDimension, int targetDimension, Set<ClusterConformation> rootConformations) {
        persistenceService.createUnauthenticatedConnection();

        Set<ClusterConformation> conformations = persistenceService.findConformationsByAtomsNumber(startingDimension);
        if (conformations.isEmpty()) {
            // If conformations of startingDimension have not been persisted, start fom the 'root' nodes
            startSearchFromNodes(rootConformations, targetDimension);
        } else {
            startSearchFromNodes(conformations, targetDimension);
        }
    }

    public void terminateSearch() {
        persistenceService.closeUnauthenticatedConnection();
    }

    public void startSearchFromRoot(ClusterConformation root, int targetDimension) {
        Set<ClusterConformation> children = conformationGenerator.generateChildConformations(root);
        startSearchFromNodes(children, targetDimension);
    }

    public void startSearchFromNodes(Set<ClusterConformation> children, int targetDimension) {
        SortedSet<ClusterConformation> filteredGrandChildren = new TreeSet<ClusterConformation>();
        int currentAtomsNumber = children.iterator().next().getAtomsNumber();
        if (currentAtomsNumber < targetDimension) {
            SortedSet<ClusterConformation> grandChildren = new TreeSet<ClusterConformation>();
            for (ClusterConformation child : children) {
                SortedSet<ClusterConformation> commonChildGrandChildren = conformationGenerator
                        .generateChildConformations(child);
                for (ClusterConformation commonChildGrandChild : commonChildGrandChildren) {
                    filterPipeline.applyIterativeConformationFilters(commonChildGrandChild, filteredGrandChildren);
                }
                LOGGER.debug("Applied iterative filters to all child conformations of {}: current 'fit' conformations number:"
                                + " {}",
                        child, filteredGrandChildren.size());
                LOGGER.debug(LoggingUtils.STRING_DIVIDER);
                grandChildren.addAll(filteredGrandChildren);
            }
            filterPipeline.applyBulkConformationFilters(grandChildren);

            //---------------------------------------------------------------
/*            for (ClusterConformation grandChild : grandChildren) {
                logStatistics(grandChild);
            }*/
            //---------------------------------------------------------------

            // LOGGER.debug("Applied bulk filters to conformations: reduced positions number to {}", grandChildren.size());
            LOGGER.debug("Applied filters to all conformations with N = {}, reduced conformations number to {}",
                    currentAtomsNumber + 1, grandChildren.size());

            logGOStatistics(grandChildren.first());
            LOGGER.debug(LoggingUtils.WIDER_STRING_DIVIDER);

            // Persists all grandchildren
            //persistenceService.bulkInsertConformations(grandChildren);

            startSearchFromNodes(grandChildren, targetDimension);
        }
    }

    private void logStatistics(ClusterConformation conformation) {
        NearestNeighbourParameters nnParams = NearestNeighbourAnalyzer
                .analyzeNearestNeighbourContacts(conformation);
        LOGGER.debug("Conformation parameters: potential - {}, relative nn distance - {}, nn count - {}:",
                conformation.getPotentialValue(), nnParams.getNNRelativeDistance(),
                nnParams.getNNCount());
    }

    private void logGOStatistics(ClusterConformation conformation) {
        String lineage = conformation.getPotentialValue() + "->";
        ClusterConformation parent = conformation.getParent();
        while (parent != null) {
            lineage += parent.getPotentialValue() + " ->";
            parent = parent.getParent();
        }
        LOGGER.debug("Global optimum LINEAGE: {}", lineage);
    }

    public static void main(String[] args) {
        FilterPipeline filterPipeline = new FilterPipeline();

        // Registers position filters
        filterPipeline.registerFilter(new PositionFilterByDistanceVector());
        //filterPipeline.registerFilter(new PositionFilterByPotentialVariance());
        //filterPipeline.registerFilter(new PositionFilterByRelativePotentialVariance());
        //filterPipeline.registerFilter(new PositionFilterByCutOff(3));

        // Registers conformation filters
        filterPipeline.registerFilter(new ConformationFilterByContributionArray());
        //filterPipeline.registerFilter(new ConformationFilterByNearestNeighbours());
        //filterPipeline.registerFilter(new ConformationFilterByPotentialVariance());

        // Creates root conformations to start search from
        MorseClusterConformation morseCluster7A = MorseConformations.createMorseConformation7A();
        MorseClusterConformation morseCluster7XB = MorseConformations.createMorseConformation7XB();
        Set<ClusterConformation> rootConformations = new TreeSet<ClusterConformation>();
        rootConformations.add(morseCluster7A);
        rootConformations.add(morseCluster7XB);

        ClusterSearchTreeBuilder searchTreeBuilder = new ClusterSearchTreeBuilder(
                filterPipeline, new MorseClusterConformationGenerator(
                filterPipeline
        ));

        searchTreeBuilder.startSearch(STARTING_DIMENSION, TARGET_DIMENSION, rootConformations);
        searchTreeBuilder.terminateSearch();
    }
}
