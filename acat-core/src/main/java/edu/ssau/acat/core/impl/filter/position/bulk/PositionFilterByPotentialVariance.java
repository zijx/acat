package edu.ssau.acat.core.impl.filter.position.bulk;

import java.util.Iterator;
import java.util.SortedSet;

import edu.ssau.acat.core.api.filter.AbstractFilter;
import edu.ssau.acat.core.api.filter.position.BulkPositionFilter;
import edu.ssau.acat.core.impl.conformation.WeightedPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PositionFilterByPotentialVariance extends AbstractFilter implements BulkPositionFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionFilterByPotentialVariance.class);

    public static final double EMPIRICAL_VARIANCE_VALUE = 3.1;

    public double potentialVariance;

    public PositionFilterByPotentialVariance() {
        this.potentialVariance = EMPIRICAL_VARIANCE_VALUE;
        setConfiguration();
    }

    public PositionFilterByPotentialVariance(double potentialVariance) {
        this.potentialVariance = potentialVariance;
        setConfiguration();
    }

    @Override
    public boolean applyFilter(SortedSet<WeightedPosition> in) {
        int removedElementsCount = 0;
        Iterator<WeightedPosition> positionIterator = in.iterator();
        double recordPotential = positionIterator.next().getWeight();
        while (positionIterator.hasNext()) {
            WeightedPosition position = positionIterator.next();
            if (Math.abs(recordPotential - position.getWeight()) > EMPIRICAL_VARIANCE_VALUE) {
                positionIterator.remove();
                //in.remove(position);
                removedElementsCount++;
            }
        }
        LOGGER.debug("Bulk filter {} filtered out {} candidates", this.getClass().getSimpleName(), removedElementsCount);
        return removedElementsCount > 0;
    }
}
